<?php 

class Server {

  //global $conn = "";

  public static function connexion(){
    
    //$conn = new mysqli("localhost", "root", "", "tranvoyage");
     /**Verfication de la connexion*/
     try
     {
     // local 
      $conn = new PDO('mysql:host=localhost;dbname=db_tranvoyage', 'root', '');
     // $conn = new PDO('mysql:host=91.216.107.183;dbname=tranv1550213', 'tranv1550213', 'y4inoby1tv');
     //ligne
     //$con = new PDO('mysql:host=localhost;dbname=arobasep_dolec', 'admin_baskoul', 'adbase123@@');
     
     }
     catch(Exception $e)
     {
     echo 'Erreur : '.$e->getMessage().'<br />';
     echo 'N° : '.$e->getCode();
     }



     return $conn;
     

  }

 
  public function search($query, $conn){
  
  $query = "SELECT * FROM utilisateurs WHERE 1";

   $res = $conn->query($query);
   $email = "";
while ($row = $res->fetch()) {
    $nom = $row['nom'];
    $email = $row['email'];
    //$all = $row;
}
   return $email;
  }


  public static function create($query, $conn, $execution){

    $save = $conn->query($query);
      if ($save) {
        return $save;
      } else { 
        die($conn->error);
      }
    
   
  }

  public static function Allquery($query, $con){
   
    $save = $con->query($query)->fetchAll(PDO::FETCH_ASSOC);
   // $save = $saves->fetchAll();fetchAll(PDO::FETCH_ASSOC)
      if ($save) {
        return $save;
      } else { 
        return 'erreur';
      }
    
  }
  

  public static function verifAttribute(){
    $con = Server::connexion();
    $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $con;
  }

  public static function recupAdressIp($date_creat,$lib_trace){
    $con = Server::connexion();
    $ip = $_SERVER['REMOTE_ADDR'];
    $port = $_SERVER['REMOTE_PORT'];
    $adresse='Adresse IP: '.$ip.' Port: '.$port;
    if (!isset($_SESSION['id_session_user'])) {
     $user_connecte = "client";
    } else {
      $user_connecte = $_SESSION['id_session_user'];
    }
 $req=$con->prepare("INSERT INTO trace(lib_trace, date_trace, adresse_ip, secur) VALUES (:A, :B, :C, :D)");
 $req->execute(array('A'=>$lib_trace, 'B'=>$date_creat, 'C'=>$adresse, 'D'=>$user_connecte));	
}


public static function strtoupperFr($string) {
  $string = strtoupper($string);
  $string = str_replace(
     array('é', 'è', 'ê', 'ë', 'à', 'â', 'î', 'ï', 'ô', 'ù', 'û'),
     array('É', 'È', 'Ê', 'Ë', 'À', 'Â', 'Î', 'Ï', 'Ô', 'Ù', 'Û'),
     $string
  );
  return $string;
 }

 // Convertit une date ou un timestamp en français
public static function dateToFrench($date, $format) 
{
    $english_days = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
    $french_days = array('lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche');
    $english_months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
    $french_months = array('janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre');
    return str_replace($english_months, $french_months, str_replace($english_days, $french_days, date($format, strtotime($date) ) ) );
}

public static function ConvertDateInFrench($dat){
	    
  $date_explode = explode("-", $dat);
  $jour=$date_explode[2];
  $mois=$date_explode[1];
  $annee=$date_explode[0];
  
  $newTimestamp = mktime(12,0,0,$mois,$jour,$annee); // Cr&eacute;&eacute; le timestamp pour ta date (a midi)
  
  // Ensuite tu fais un tableau avec les jours en Français
  $Jour = array("Dim","Lun","Mar","Mer","Jeu","Ven","Sam");
  
  
  // Pareil pour les mois en FR
  $Mois = array("","Jan","F&eacute;v","Mar","Avr","Mai","Jui","Jui","Ao&ucirc;","Sep","Oct","Nov","D&eacute;c");
  
  
  // Enfin tu affiche le libell&eacute; du jour
  $j= $Jour[date("w", $newTimestamp)];
  
  // Et le libell&eacute; du mois
  $m= $Mois[date("n", $newTimestamp)];
  
  $result=$j.' '.$jour.' '.$m.' '.$annee;
  return $result;
}


public static function soldeSMS($mail,$key) {
    $url = "http://www.letexto.com/get_sold/user/".$mail."/secret/".$key;
    $curl = curl_init();
    curl_setopt_array($curl, array(
     CURLOPT_URL => $url ,
     CURLOPT_RETURNTRANSFER => true,
     CURLOPT_ENCODING => "",
     CURLOPT_MAXREDIRS => 10,
     CURLOPT_TIMEOUT => 30,
     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
     CURLOPT_CUSTOMREQUEST =>"GET",
     CURLOPT_HTTPHEADER => array("cache-control: no-cache"),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    return $response;

  }

 
  public static function Sendsms($msg,$tel,$sender)
  {
    // Filtrer le messages
         $nvMsg = str_replace('à','a', $msg); 
         $nvMsg = str_replace('á','a', $nvMsg);
         $nvMsg = str_replace('â','a', $nvMsg);
         $nvMsg = str_replace('ç','c', $nvMsg);
         $nvMsg = str_replace('è','e', $nvMsg);
         $nvMsg = str_replace('é','e', $nvMsg);
         $nvMsg = str_replace('ê','e', $nvMsg);
         $nvMsg = str_replace('ë','e', $nvMsg);
         $nvMsg = str_replace('ù','u', $nvMsg);
         $nvMsg = str_replace('ù','u', $nvMsg);
         $nvMsg = str_replace('ü','u', $nvMsg);
         $nvMsg = str_replace('û','u', $nvMsg);
         $nvMsg = str_replace('ô','o', $nvMsg);
         $nvMsg = str_replace('î','i', $nvMsg);
         $curl = curl_init();
         $datas = [
            //"email"=>getSettingByName('sms_mail'), //la fonction n'est pas définir . 
           // "secret"=>getSettingByName('sms_secret'),
            "email"=>"Cherubin0225@gmail.com",
            "secret"=>"QLkTUwOia0KBx2aMQ@X6r9cQNGXkf4SXRUwuSgAw",
            "message"=>$nvMsg,
            "receiver"=>$tel,
            "sender"=>$sender,
            "cltmsgid"=>"1"
         ];
          $response = $tel.'/';
         curl_setopt_array($curl, array(
           CURLOPT_URL => "www.letexto.com/sendCampaign",
           CURLOPT_RETURNTRANSFER => true,
           CURLOPT_ENCODING => "",
           CURLOPT_MAXREDIRS => 10,
           CURLOPT_TIMEOUT => 0,
           CURLOPT_FOLLOWLOCATION => true,
           CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
           CURLOPT_CUSTOMREQUEST => "POST",
           CURLOPT_POSTFIELDS =>json_encode($datas),
           CURLOPT_HTTPHEADER => array(
             "Content-Type: application/json",
            ),
         ));
         $response = curl_exec($curl);
         curl_close($curl);
         return $response;  
  }





}

?>
