<?php 

$con = Server::connexion();
$ticket_id = $_GET['res'];
$req = "SELECT * FROM destination  
        LEFT JOIN compagnie ON compagnie.id_compagnie=destination.compagnie_destination_id 
        LEFT JOIN gare ON destination.depart_gare_id=gare.id_gare WHERE  destination.id_destination='".$ticket_id."' ";
$prix_ticket_recup= $con->query($req);

$prix_ticket= $prix_ticket_recup->fetch();

?>

<section class="top_place" style="margin-top:50px;margin-bottom:50px;">
        <div class="container">
            <!-- <div class="row">
                <div class="col-xl-6">
                    <table>
                        <thead>
                            <tr>
                                <th colspan="2">The table header</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>The table body</td>
                                <td>with two columns</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div> -->
        <div  style="border:5px solid #1E8449;min-height:auto;border-radius:5px;">    
            <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link nav_achat active" id="tickets_achat"  href="#">Achat de tickets</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav_ticket"  id="mes_tickets"   href="#">Mes tickets</a>
                    </li>
                    <!-- <li class="nav-item">
                        <a class="nav-link active" href="#">Link</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled" href="#">Disabled</a>
                    </li> -->
            </ul>

            <div class="col-lg-12 voir_choix_ticket" style="margin-top: 10px;"> 
            <form action="#" id="form_verif_ticket">
            <div class="row">
            <div class="col-lg-3">
                    <label for="verif">Code ticket <i class="fas fa-ticket"></i></label> 
                    <input type="text" placeholder="Saisir le code du ticket"  name="verif_ticket" id="verif_ticket" class="form-control" style="background-color:#FDF2E9;color:#000;font-weight:bold;border:3px solid #000;" required>
            </div>
                    <div class="col-lg-3" style="margin-top: 29px;">
                            <button type="submit" id="envoie_verif" class="btn btn-md btn-success">Rechercher <i id="icon_verif" class="fa fa-search"></i></button>
                    </div> 
            </div>         
            </form><br>
            <div class="col-lg-12" style="margin-bottom:10px;margin-top: 10px;" id="load_avant_tab" hidden><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>
            <div class="col-lg-12" style="margin-top: 10px;"> 
                <table id="example" class="table table-striped table-bordered nowrap" style="width:100%;margin-top:5px;">              
                    <thead style="background:#2CABDC;">
                        <tr style="text-align: center;">
                            <th>code ticket</th>
                            <th>Nombre de ticket</th>
                            <th>Prix unitaire du ticket</th>
                            <th>Compagnie</th>
                            <th>Départ</th>
                            <th>Destination</th>
                            <th>Total (F.CFA)</th>
                        </tr>
                    </thead>
                    <tbody> 
                    </tbody>
                    <tbody class="voir_ticket_verif">
                      
                    </tbody>
                    <tfoot>
                        <tr>
                
                        </tr>
                    </tfoot>
                </table>
            </div>
            
            
            </div> 
     

            <div class="col-lg-12 cache_choix_ticket"> 
               <form action="#" id="form_compte_ticket">
                     <div class="row">
                     
                           <div class="col-lg-2">
                                   <label for="nombre">Choisir le nombre</label>
                                   <select name="nbre_tickets" id="nbre_tickets" style="background-color:#FDF2E9;color:#000;font-weight:bold;border:3px solid #000;" class="form-control">
                                       <?php for($i=1;$i<11;$i++){ ?>
                                            <option value="<?php echo $i;?>"><?php echo $i; ?></option>
                                       <?php } ?>
                                   </select>
                           </div>
                           <div class="col-lg-2">
                                   <label for="nombre">Prix total (F.CFA)</label>
                                   <input type="number" value="<?php echo $prix_ticket['tarif_prix'];  ?>" name="total_tickets" id="total_tickets" class="form-control" min="1" readonly="true" style="background-color:green;color:white;font-weight:bold;font-size:16px;text-align:center;">
                           </div>
                           <div class="col-lg-3">
                                   <label for="numero">Téléphone <i class="fa fa-question-circle" aria-hidden="true" title="Veuillez cliquer sur ce bouton d'information" onclick="info();" style="color:red;cursor:pointer;"></i></label>
                                   <input type="tel"  name="tel_ticket" id="tel_ticket" class="form-control"  style="background-color:#FDF2E9;color:#000;font-weight:bold;border:3px solid #000;" required>
                           </div>
                           <div class="col-lg-3">
                                   <label for="email">Email (optionnel)</label>
                                   <input type="email"  name="email" id="email" class="form-control" min="1"   style="background-color:#FDF2E9;color:#000;font-weight:bold;border:3px solid #000;">
                           </div>
                           <input type="hidden"  name="date_depart" value="<?php echo $prix_ticket['date_depart'];  ?>">
                           <input type="hidden"  name="heure_depart" value="<?php echo $prix_ticket['heure_depart'];  ?>">
                           <input type="hidden"  name="depart_id" value="<?php echo $prix_ticket['depart_gare_id'];  ?>">
                           <input type="hidden"  name="arrive_id" value="<?php echo $prix_ticket['arrive_gare_id'];  ?>">
                           <input type="hidden"  name="tarif_prix" value="<?php echo $prix_ticket['tarif_prix'];  ?>">
                           <input type="hidden"  name="compagnie_destination_id" value="<?php echo $prix_ticket['compagnie_destination_id'];  ?>">
                           
                           <div class="col-lg-2" style="margin-top: 29px;">
                                  <button type="submit" id="envoie_paiement" class="btn btn-md btn-success">Valider <i id="icon_paiment" class="fa fa-check"></i></button>
                           </div>

                </form> 
                           
                           <div class="col-lg-4 vu_code_validation" style="margin-top: 29px;" id="">
                                   <label for="code_validation" style="background-color:green;padding:10px;color:#fff;">Veuillez saisir ici le code de validation reçu <i class="fa fa-key" aria-hidden="true" ></i></label>
                                   <input type="text"  name="code_validation" id="code_validation" placeholder="Veuillez saisir le code reçu" class="form-control"  style="background-color:#FDF2E9;color:#000;font-weight:bold;border:3px solid #000;">
                           </div>

                           <div class="col-lg-3 vu_code_validation" style="margin-top:78px;">
                                  <button type="button" id="envoie_code_valide" class="btn btn-md btn-info">Confirmer <i id="icon_paiment_validation" class="fa fa-check"></i></button> 
                           </div>

                           <div class="col-lg-4" style="margin-top:50px;z-index:-1;" id="load_avant_ticket"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>

                           </div>
                  
                     <br>


                    
            </div>
           
     </div>

            
        


        </div>
</section>


<!-- <section class="top_place section_padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6">
                    <div class="section_tittle text-center">
                        <h2>Aucun résultat trouvé <i class="fa fa-search fa-2x"></i></h2>
                    </div>
                </div>

            </div>

        </div>
    </section> -->