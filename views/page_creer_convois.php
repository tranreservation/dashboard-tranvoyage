<section class="top_place" style="margin-top:50px;margin-bottom:50px;">
    <div class="container">
        <form>
            <div class="form-group">
                <label for="exampleFormControlInput1">Nom</label>
                <input type="text" class="form-control" placeholder="Nom">
            </div>
            <div class="form-group">
                <label for="exampleFormControlInput1">Prénom</label>
                <input type="text" class="form-control" placeholder="Nom">
            </div>
            <div class="form-group">
                <label for="exampleFormControlInput1">Téléphone</label>
                <input type="text" class="form-control" placeholder="Téléphone">
            </div>
            <div class="form-group">
                <label for="exampleFormControlSelect1">Example select</label>
                <select class="form-control" id="exampleFormControlSelect1">
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                </select>
            </div>
            <div class="form-group">
                <label for="exampleFormControlSelect2">Example multiple select</label>
                <select multiple class="form-control" id="exampleFormControlSelect2">
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                </select>
            </div>
            <div class="form-group">
                <label for="exampleFormControlTextarea1">Donner une description du convois</label>
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="5"></textarea>
            </div>
        </form>
    </div>
</section>