<section class="top_place" style="margin-top:50px;margin-bottom:50px;">
    <div class="container">
        <div class="col-lg-12" style="margin-top: 10px;border:5px solid #2CABDC;min-height:auto;border-radius:5px;">
            <form action="#" id="form_verif_ticket">
                <div class="row">
                    <div class="col-lg-3">
                        <br>
                        <input type="text" placeholder="Saisir le code du ticket" name="verif_ticket" id="verif_ticket" class="form-control" style="background-color:#FDF2E9;color:#000;font-weight:bold;border:3px solid #000;" required>
                    </div>
                    <div class="col-lg-3" style="margin-top: 23px;">
                        <button type="submit" id="envoie_verif" class="btn btn-md btn-success">Rechercher <i id="icon_verif" class="fa fa-search"></i></button>
                    </div>
                </div>
            </form><br>
            <div class="col-lg-12" style="margin-top: 10px;">
                <table id="example" class="table table-striped table-bordered nowrap" style="width:100%;margin-top:5px;">
                    <thead style="background:#2CABDC;">
                        <tr style="text-align: center;">
                            <th>code ticket</th>
                            <th>Nombre de ticket</th>
                            <th>Prix unitaire du ticket</th>
                            <th>Compagnie</th>
                            <th>Départ</th>
                            <th>Destination</th>
                            <th>Total (F.CFA)</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tbody class="voir_ticket_verif">

                    </tbody>
                    <tfoot>
                        <tr>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
</section>