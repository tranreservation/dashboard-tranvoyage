<?php
require_once 'cinetpay/cinetpay.php';
require_once 'cinetpay/commande.php';

$con = Server::connexion();
?>

<style>
    .cinetpay-button {
        /*color: #fff !important;
        background-color: #218838 !important;
        border-color: #1e7e34 !important;*/
        float: right;
    }

    .btn-success{
        color: #fff !important;
    }

    #goCinetPay{
        padding: 0px;
    }

    #cinetpay-button{
        padding-top: 4px;
    }
</style>

<section class="top_place" style="margin-top:50px;margin-bottom:50px;">

    <div class="container">
        <div class="row">
            <div class="offset-md-3 col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <h3 id="title-reserver">Reserver un ticket</h3>
                    </div>
                    <div class="col-xl-12">

                        <?php
                            if ($_GET['code_ticket'] != "" && $_GET['montant_total_ticket'] != "" && $_GET['nombre_ticket']) {

                                date_default_timezone_set("Africa/Abidjan");
                                $commande = new Commande();
                                try {
                                    $id_transaction = Cinetpay::generateTransId();
                                    $description_du_paiement = "Mon ticket de ref: $id_transaction";
                                    $date_transaction = date("Y-m-d H:i:s");
                                    // Montant minimun est de 5 francs sur CinetPay
                                    $montant_a_payer = $_GET['montant_total_ticket'];

                                    // Mettez ici une information qui vous permettra d'identifier de fa�on unique le payeur
                                    $identifiant_du_payeur =  $_GET['code_ticket'];

                                    $nombre_ticket = $_GET['nombre_ticket'];
                                    $mont_ticket_unitaire = $_GET['mont_ticket_unitaire'];
                                    $telephone = $_GET['telephone'];
                                    $email = $_GET['email'];
                                    $nombre_ticket = $_GET['nombre_ticket'];

                                    // Ajout de la transaction de cinetpay dans paiement
                                    $code_ticket = $_GET['code_ticket'];
                                    $req2 = $con->prepare("UPDATE paiement SET id_transaction=:id_transaction WHERE code_ticket=:code_ticket");
                                    $req2->execute(array(
                                        'code_ticket' => $code_ticket,
                                        'id_transaction' => $id_transaction
                                    ));
                        ?>

                        <div class="mb-3">
                            <label class="form-label">Le nombre de ticket</label>
                            <input type="text" value="<?=$nombre_ticket?>" class="form-control" readonly>
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Prix unitaire</label>
                            <input type="text" value="<?=$mont_ticket_unitaire?>" class="form-control" readonly>
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Prix total (F.CFA)</label>
                            <input type="text" value="<?=$montant_a_payer?>" class="form-control" readonly>
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Téléphone </label>
                            <input type="tel" value="<?=$telephone?>" class="form-control" readonly>
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Email</label>
                            <input type="email" value="<?=$email?>" class="form-control" readonly>
                        </div>

                        <?php
                            

                                    //Veuillez entrer votre apiKey
                                    $apiKey = "2038304885ffddd1d536dc0.56779503";
                                    //Veuillez entrer votre siteId
                                    $site_id = "179731"; // à remplacer

                                    //platform ,  utiliser PROD si vous avez cr�� votre compte sur www.cinetpay.com  ou TEST si vous avez cr�� votre compte sur www.sandbox.cinetpay.com
                                    $plateform = "PROD";

                                    //la version ,  utilis� V1 si vous voulez utiliser la version 1 de l'api 
                                    $version = "V2";

                                    // nom du formulaire CinetPay
                                    $formName = "goCinetPay";
                                    // Les liens CinetPay 
                                    $notify_url = '';
                                    // $return_url = "https://www.monconfort.ci/controlleurs/valider_panier_online.php?id_user={$_SESSION['id_user']}&livraison=$livraison";
                                    // $return_url = "http://tranvoyagetest.ecademy.ci/return.php?id_user={$_GET['code_ticket']}&livraison={$_GET['nombre_ticket']}";
                                    // $return_url = "http://tranvoyagetest.ecademy.ci/return.php?id_transaction={$id_transaction}";
                                    $return_url = "http://tranvoyage.com/return.php?id_transaction={$id_transaction}";
                                    $cancel_url = "http://tranvoyage.com/";
                                    // Configuration du bouton
                                    $btnType = 2; //1-5
                                    $btnSize = 'large'; // 'small' pour reduire la taille du bouton, 'large' pour une taille moyenne ou 'larger' pour  une taille plus grande

                                    // Enregistrement de la commande dans notre BD
                                    $commande->setTransId($id_transaction);
                                    $montant_a_payer = 100;
                                    $commande->setMontant($montant_a_payer);
                                    $commande->create();

                                    // Param�trage du panier CinetPay et affichage du formulaire
                                    $CinetPay = new CinetPay($site_id, $apiKey, $plateform, $version);
                                    $CinetPay->setTransId($id_transaction)
                                        ->setDesignation($description_du_paiement)
                                        ->setTransDate($date_transaction)
                                        ->setAmount($montant_a_payer)
                                        ->setDebug(false) // Valoris� � true, si vous voulez activer le mode debug sur cinetpay afin d'afficher toutes les variables envoy�es chez CinetPay
                                        ->setCustom($identifiant_du_payeur) // optional
                                        ->setNotifyUrl($notify_url) // optional
                                        ->setReturnUrl($return_url) // optional
                                        ->setCancelUrl($cancel_url) // optional
                                        ->displayPayButton($formName, $btnType, $btnSize);

                                        echo"<script language='javascript'>
                                            document.getElementById('goCinetPay').innerHTML += \"<a class='btn btn-md btn-success float-left'>Retour</a>\";
                                            // document.getElementById('goCinetPay').style.color = 'padding: 0px';
                                            document.getElementById('goCinetPay').classList.add('col-lg-12');
                                            document.getElementById('goCinetPay').classList.remove('cinetpay-button');
                                            /*document.getElementsByClassName('cinetpay-button large').classList.add('btn-success');*/
                                            // console.log('Text ->'+document.getElementsByClassName('cinetpay-button large')[0].className);
                                            document.getElementsByClassName('cinetpay-button large')[0].setAttribute('id', 'cinetpay-button');
                                            // document.getElementById('cinetpay-button').classList.add(' btn btn-md btn-success');
                                            document.getElementsByClassName('cinetpay-button large')[0].className += ' btn btn-md btn-success';
                                            let html = document.getElementsByClassName('btn btn-md btn-success float-left')[0].innerHTML;
                                            html = \"<i class='fa fa-arrow-left'></i> \" + html;
                                            document.getElementsByClassName('btn btn-md btn-success float-left')[0].innerHTML = html;
                                            document.getElementById('cinetpay-button').innerHTML += \"<i class='fas fa-shopping-cart'></i>\";
                                        </script>";
                                } catch (Exception $e) {
                                    die("une erreur");
                                    // Une erreur est survenue
                                    echo $e->getMessage();
                                }
                            }
                        ?>


                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

<script>
</script>