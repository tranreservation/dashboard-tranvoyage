<section class="top_place" style="margin-top:50px;margin-bottom:50px;">
       <div class="container qui-sommes-nous">
              <div class="col-lg-12" style="margin-top: 10px;min-height:auto;border-radius:5px;">
                     <h3>Qui sommes-nous ?</h3>
                     <div style="color:#000;background:#fff;padding:5px;">
                            <p>
                                   <span style="color:#000;font-weight:bold;">Tranvoyage </span> du préfixe Tran (distribuer en langue malinké) et du suffixe voyage,
                                   est une agence de voyage en ligne qui accompagne les compagnies de transport à booster
                                   leur chiffre d’affaires, par la vente de trajets en ligne.
                            </p>
                            <br>
                            <p>
                                   Chez Tranvoyage.com,
                                   nous prenons une approche unique du voyage. Notre plate-forme pionnière vous permet de trouver
                                   les options de voyage les plus rapides, les moins chères et les meilleures en car,
                                   en train et en vol vers des milliers de villes,
                                   villages et villages à travers la Côte-d’Ivoire et dans la sous-région.
                                   Cela se traduit par des économies — sur l’argent et le temps — pour vous !
                            </p>
                     </div>
              </div>
       </div>
</section>