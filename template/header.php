<header class="main_menu">
    <div class="sub_menu">
        <div class="container">
            <div class="row justify-content-center">
                <nav class="navbar navbar-expand-lg navbar-light justify-content-between">
                    <div class="col-md-3">
                        <a class="navbar-brand" href="index.php"> <img src="img/logo.jpg" width="380px" alt="logo" id="logo-navbar"> </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                    </div>
                    <div class="col-md-9">
                        <div class="collapse navbar-collapse main-menu-item" id="navbarSupportedContent">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link" href="index.php">Acceuil</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="recherche_ticket.php">Rechercher un ticket</a>
                                </li>
                                <!-- <li class="nav-item">
                                    <a class="nav-link" href="recherche_ticket.php">Liste des convois</a>
                                </li> -->
                                <!-- <li class="nav-item">
                                    <a class="nav-link" href="creer_convoi.php">Créer un convoi</a>
                                </li> -->
                                <li class="nav-item">
                                    <a class="nav-link" href="info.php">Qui sommes-nous ?</a>
                                </li>
                                <!-- <li class="nav-item">
                                    <a class="nav-link" href="contact.php">Contacts</a>
                                </li> -->
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>