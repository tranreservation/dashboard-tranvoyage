<header class="main_menu">
<div class="sub_menu">
<div class="container">
<div class="row">
<div class="col-lg-6 col-sm-12 col-md-6">
<div class="sub_menu_right_content">
<span style="color: #7DBF50;">Tranvoyage</span>

</div>
</div>
<div class="col-lg-6 col-sm-12 col-md-6">
<div class="sub_menu_social_icon">
<a target="_blank" href="https://www.facebook.com/tranvoyage"><i class="flaticon-facebook"></i></a>
<a href="#"><i class="flaticon-twitter"></i></a>
<a target="_blank" href="https://www.linkedin.com/company/trancom"><i class="flaticon-skype"></i></a>
<a target="_blank" href="https://www.instagrame.com/tranvoyage/"><i class="flaticon-instagram"></i></a>
<span><i class="flaticon-phone-call"></i>+225 07 7948 1690</a></span>
</div>
</div>
</div>
</div>
</div>
<div class="main_menu_iner">
<div class="container">
<div class="row align-items-center ">
<div class="col-lg-12">
<nav class="navbar navbar-expand-lg navbar-light justify-content-between">
<a class="navbar-brand" href="index.php"> <img src="img/logo.jpg" height="65px" width="380px" alt="logo"> </a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse main-menu-item justify-content-center" id="navbarSupportedContent">
<ul class="navbar-nav">
<li class="nav-item">
<a class="nav-link" href="index.php">Gares</a>
</li>
<!-- <li class="nav-item">
<a class="nav-link" href="#">A propos</a>
</li> -->
<li class="nav-item">
<a class="nav-link" href="#">Contacts</a>
<!-- </li>
<li class="nav-item"><a class="nav-link" href="#">Blog</a>
</li> -->
</ul>
</div>
<a href="#" class="btn_1 d-none d-lg-block" style="background-color:#7DBF50;">Connexion</a>
</nav>
</div>
</div>
</div>
</div>
</header>

