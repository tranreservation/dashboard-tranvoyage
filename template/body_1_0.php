<section class="booking_part">
<div class="container">
<div class="row">
<div class="col-lg-12">
<div class="booking_menu">
<ul class="nav nav-tabs" id="myTab" role="tablist">
<li class="nav-item">
<a class="nav-link active" id="hotel-tab" data-toggle="tab" href="#hotel" role="tab" aria-controls="hotel" aria-selected="true"><i class="fas fa-bus fa-8x" style="color: #2cabdc;"></i></a>
</li>

</ul>
</div>
</div>
<div class="col-lg-12">
<div class="booking_content" style="border:3px solid #7DBF50;border-radius:5px;">
<div class="tab-content" id="myTabContent">
<div class="tab-pane fade show active" id="hotel" role="tabpanel" aria-labelledby="hotel-tab">
<div class="booking_form">
<form action="#">
<div class="form-row">
<div class="form_colum">
<select class="" id="gare_dep"  style="border-radius:5px;border:5px solid #7DBF50;" >
    <?php       
    $requete = "SELECT * FROM gare WHERE id_gare!='0' ";
     $con = Server::connexion(); 
     $gare = Server::Allquery($requete, $con); 
    ?>
<option value="">choisir gare de départ</option>
<?php foreach($gare as $gares) {  ?>
<option value="<?php echo $gares['id_gare']; ?>"><?php echo Server::strtoupperFr($gares['nom_gare']); ?></option>
<?php }  ?>
</select>
</div>
<div class="form_colum">
<select class="" id="gare_arr"   style="border-radius:5px;border:5px solid #7DBF50;">
<option value="">choisir gare d'arrivée </option>
<?php foreach($gare as $gares) {  ?>
<option value="<?php echo $gares['id_gare']; ?>"><?php echo Server::strtoupperFr($gares['nom_gare']); ?></option>
<?php }  ?>
</select>
</div>
<div class="form_colum" style="margin-top:-4px;" >
 <input id="datepicker_2" style="border-radius:5px;border:5px solid #7DBF50;" placeholder="Choisir la date de départ" readonly="true">
</div>
<!-- <div class="form_colum">
<select class="nc_select">
<option selected>Persone </option>
<option value="1">One</option>
<option value="2">Two</option>
<option value="3">Three</option>
</select>
</div> -->
<div class="form_btn">
<a href="#" class="btn_1" id="btn_envoie_rech" style="background:#7DBF50;">Rechercher <i class="fa fa-search"></i></a>
</div>
</div>
</form>
</div>
</div>


</div>
</div>
</div>
</div>
</div>
</section>




<section class="top_place section_padding">
        <div class="container">
            <!-- <div class="row justify-content-center">
                <div class="col-xl-6">
                    <div class="section_tittle text-center">
                        <h2>Aucun résultat trouvé <i class="fa fa-search fa-2x"></i></h2>
                    </div>
                </div>

            </div> -->

            <div class="row" style="margin-top:-15px;">
                <div class="col-md-12">
                    <div class="marquee-rtl">
                        <!-- le contenu défilant -->
                        <div>Des compagnies que vous aimez dans une seule et même application</div>
                    </div>
                </div>
                <br><br>
                <div class="row gars_aff"></div>
                <div class="col-md-12 chargement"></div>
                <!-- <div class="col-md-6">
                    <div class="gars_affiche">
                        <div class="row">
                            <div class="col-md-3" id="desktop1">
                            <i class="fa fa-bus fa-7x" style="padding: 10px;color:white;"></i>
                            </div>
                            <div class="col-md-3" id="mobile1">
                               <i class="fa fa-bus fa-3x" style="padding: 10px;color:white;"></i> <span style="margin-top:-10px;"><a href="#" style="text-decoration: none;margin-top:-20px;color:#000;">Lundi 05 Avril 2021</a> <span>
                                <a href="reservation.php" class="btn" type="button " style="margin-top:-20px;background-color:#229954;color: white;border:2px solid white;height:30px;margin-bottom:3px;border-radius:15px;font-size: 10px;font-weight: bold; ">Reserver le ticket</a> 
                            </div>
                            <div class="col-md-9">
                                <div id="desktop2">
                                    <span style="font-family: Arial, Helvetica, sans-serif;font-size:1.2em;font-weight: bold;">
									      Lundi, 05 Avril 2021
								    </span>
                                    <a href="reservation.php" class="btn " type="button " style="background-color:#229954;color: white;border:2px solid white;height:30px;margin-bottom:3px;border-radius:15px;font-size: 10px;font-weight: bold;margin-left:120px;; ">Reserver le ticket</a>
                                </div>
                                <div class="col-md-12" style="border-bottom:3px solid white; "></div>
                                <div class="col-md-12" style="background: #14d5ee;">
                                    <span style="font-family: Arial, Helvetica, sans-serif;font-weight: bold;margin-left:-0.90em; ">
									 <span style="font-size:1em;color: #1E8449; ">Abidjan - Bouaké | Départ: </span> <span style="font-size:1em;color:white; ">09h30 min</span>
                                    </span>
                                </div>
                                <div class="col-md-12" style="border-bottom:3px solid white; "></div>
                                <div class="col-md-12" style="background: #7DBF50;">
                                    <span style="font-family: Arial, Helvetica, sans-serif;font-weight: bold;margin-left:-0.90em; ">
									 <span style="font-size:12px;">Tarif:</span> <span style="font-size:12px;color:white;">5000 f.cfa</span> &nbsp;&nbsp;
                                    <span style="font-size:12px;"> Compagnie: </span><span style="font-size:12px;color:white;">Leopard transport</span>
                                    </span>
                                </div>
                                <div class="col-md-12" style="border-bottom:3px solid white; "></div>
                                <div class="col-md-12" style="background: #48C9B0;">
                                    <span style="font-family: Arial, Helvetica, sans-serif;font-weight: bold;margin-left:-0.90em; ">
                                    <span style="font-size:12px;">Gare: </span><span style="font-size:12px;color:white;">Leopard transport</span>
                                    </span>
                                </div>
                            </div>



                        </div>

                    </div>
                </div>  -->
                <!-- <div class="" ></div> -->


            </div>

        </div>
    </section>