<style>
    .booking_content{
        border:3px solid #7DBF50;
        border-radius:5px;
        display: none
    }
</style>

<?php
$requete = "SELECT * FROM gare WHERE id_gare!='0' ";
$con = Server::connexion();
$gare = Server::Allquery($requete, $con);
?>

<section class="banniere">
    <div class="container">
        <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="hotel" role="tabpanel" aria-labelledby="hotel-tab">
                    <div class="booking_form">
                        <form action="#" class="form-inline row">
                            <div class="form_group col-md-3 col-12">
                                <select class="" id="gare_dep_2">
                                    <?php       
                                        $requete = "SELECT * FROM gare WHERE id_gare!='0' ";
                                        $con = Server::connexion(); 
                                        $gare = Server::Allquery($requete, $con); 
                                    ?>
                                    <option value="">Chosir la gare de départ</option>
                                    <?php foreach($gare as $gares) {  ?>
                                        <option value="<?php echo $gares['id_gare']; ?>"><?php echo Server::strtoupperFr($gares['nom_gare']); ?></option>
                                    <?php }  ?>
                                </select>
                            </div>
                            <div class="form_group col-md-3 col-12">
                                <select class="" id="gare_arr_2">
                                    <option value="">Choisir la gare d'arrivée </option>
                                    <?php foreach($gare as $gares) {  ?>
                                        <option value="<?php echo $gares['id_gare']; ?>"><?php echo Server::strtoupperFr($gares['nom_gare']); ?></option>
                                    <?php }  ?>
                                </select>
                            </div>
                            <div class="form_group col-md-3 col-12">
                                <input id="datepicker_2" placeholder="Choisir la date de départ" readonly="true">
                            </div>
                            <!-- <div class="form-inline form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="sample-date" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" data-toggle="datepickers" data-target-name="sample-date">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </button>
                                    </span>
                                </div>
                            </div> -->
  
                            <div class="form_group col-md-3 col-12">
                                <a href="javascript:void(0)" class="btn_1" id="btn_envoie_rech" style="background:#7DBF50;"><i class="fa fa-search"></i> Rechercher </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <span class="first-span">LA LISTE DES GARES SELON LES DESTINATIONS</span>
        </div>
        <div class="col-md-12">
            <span class="two-span">Plus de besoin de vous déplacer . Vous</span> 
            <span class="three-span">pouvez acheter votre ticket </span>
            <span class="two-span">en toute sécurité </span>
            <span class="first-span">ici.</span>
        </div>
    </div>
</section>

<section class="booking_part" style="display: none;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="booking_menu"></div>
            </div>
            <div class="col-lg-12">
                <div class="booking_content">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="hotel" role="tabpanel" aria-labelledby="hotel-tab">
                            <div class="booking_form">
                                <form action="#">
                                    <div class="form-row">
                                        <div class="form_colum">
                                            <select class="" id="gare_dep" style="border-radius:5px;border:5px solid #7DBF50;">
                                                <option value="">choisir une gare de départ</option>
                                                <?php foreach ($gare as $gares) {  ?>
                                                    <option value="<?php echo $gares['id_gare']; ?>"><?php echo Server::strtoupperFr($gares['nom_gare']); ?></option>
                                                <?php }  ?>
                                            </select>
                                        </div>
                                        <div class="form_colum">
                                            <select class="" id="gare_arr" style="border-radius:5px;border:5px solid #7DBF50;">
                                                <option value="">choisir une gare d'arrivée </option>
                                                <?php foreach ($gare as $gares) {  ?>
                                                    <option value="<?php echo $gares['id_gare']; ?>"><?php echo Server::strtoupperFr($gares['nom_gare']); ?></option>
                                                <?php }  ?>
                                            </select>
                                        </div>
                                        <div class="form_colum" style="margin-top:-4px;">
                                            <input id="datepicker_2" style="border-radius:5px;border:5px solid #7DBF50;" placeholder="Choisir la date de départ" readonly="true">
                                        </div>
                                        <div class="form_btn">
                                            <a href="#" class="btn_1" id="btn_envoie_rech" style="background:#7DBF50;">Rechercher <i class="fa fa-search"></i></a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="top_place section_padding">
    <div class="container">
        <div class="row" style="margin-top:-15px;">
            <div class="col-md-12">
                <!--<div class="marquee-rtl">
                    <div>
                        <i class="fa fa-bus fa-1x" style="padding:10px;color:#000;"></i>
                        <?php
                        $requete = "SELECT * FROM annonces WHERE id_annonces!='0' ORDER BY date_creat_annonces DESC LIMIT 1";
                        $annonceste_annonces = $con->query($requete);
                        ?>
                        <?php foreach ($annonceste_annonces as $myannonce) {
                            echo stripslashes($myannonce['libelle_annonces']);
                        } ?>
                        <i class="fa fa-bus fa-1x" style="padding:10px;color:#000;"></i>
                    </div>
                </div>-->
            </div>
            <div class="col-md-12">
                <div class="row gars_aff"></div>
            </div>
            <div class="col-md-12 chargement"></div>
        </div>
    </div>
</section>