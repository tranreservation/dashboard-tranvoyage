<?php 
session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Tranvoyage</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="public/img/favicon.png" rel="icon">
  <link href="public/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,500,600,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="public/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- public/lib/raries CSS Files -->
  <link href="public/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="public/lib/animate/animate.min.css" rel="stylesheet">
  <link href="public/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="public/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="public/lib/lightbox/css/lightbox.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="public/css/style.css" rel="stylesheet">

  <!-- =======================================================
    Theme Name: Rapid
    Theme URL: https://bootstrapmade.com/rapid-multipurpose-bootstrap-business-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>
<body>
<?php 

include "./server.php";


$query = "SELECT * FROM users WHERE 1";

$resultat = new Server();

$conn = $resultat->connexion();

$affiche = $resultat->search($query, $conn);


//include "./template/head.php"; 
include "./template/header.php"; 
?>

<?php 
include "./template/body_1.php"; 
include "./template/body_2.php";
?>

<?php 
include "./template/footer.php"; 
include "./template/scripts.php";

?>

</body>
</html>



