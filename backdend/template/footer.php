<footer class="br-footer">
            <div class="footer-left">
                <div class="mg-b-2">Copyright &copy; 2020. Tranvoyage.com touts droits Reservés.</div>
                <div>Unze autre façon de payer vos tickets de voyage.</div>
            </div>
            <div class="footer-right d-flex align-items-center">
                <span class="tx-uppercase mg-r-10">Partager:</span>
                <a target="_blank" class="pd-x-5" href="#"><i class="fa fa-facebook tx-20"></i></a>
                <a target="_blank" class="pd-x-5" href="#"><i class="fa fa-twitter tx-20"></i></a>
            </div>
</footer>