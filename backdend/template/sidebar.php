<div class="br-logo"><a href="../../index.php"><span>[</span>Tranvoyage<span>]</span></a></div>
    <div class="br-sideleft overflow-y-auto">
        <label class="sidebar-label pd-x-15 mg-t-20">Navigation</label>
        <div class="br-sideleft-menu">
            <a href="../../index.php" class="br-menu-link active">
                <div class="br-menu-item">
                    <i class="fa fa-home tx-24"></i>
                    <span class="menu-item-label">Tableau de bord</span>
                </div>
                <!-- menu-item -->
            </a>
            <!-- br-menu-link -->
            <a href="#" class="br-menu-link">
                <div class="br-menu-item">
                    <i class="fa fa-envelope tx-24"></i>
                    <span class="menu-item-label">Notification</span>
                </div>
                <!-- menu-item -->
            </a>
            <!-- br-menu-link -->

            <a href="#" class="br-menu-link">
                <div class="br-menu-item">
                    <i class="fa fa-bus tx-24"></i>
                    <span class="menu-item-label">Gestion des convois</span>
                    <i class="menu-item-arrow fa fa-angle-down"></i>
                </div>
                <!-- menu-item -->
            </a>
            <!-- br-menu-link -->
            <ul class="br-menu-sub nav flex-column">
                <!-- <li class="nav-item"><a href="#" class="nav-link">Validation des Tickets</a></li> -->
                <!-- <li class="nav-item"><a href="#" class="nav-link">Les guichets</a></li> -->
                <li class="nav-item"><a href="../destination/liste_voyage.php" class="nav-link">Liste des convois</a></li>
            </ul>
            <?php if($_SESSION['role'] == '3'){ ?>
            <a href="../destination/paiement.php" class="br-menu-link">
                <div class="br-menu-item">
                    <i class="fas fa-ticket-alt tx-22"></i>
                    <span class="menu-item-label">Gestion des tickets</span>
                </div>
                <!-- menu-item -->
            </a>
            <?php } ?>
            <!-- br-menu-link -->
            <a href="#" class="br-menu-link">
                <div class="br-menu-item">
                    <i class="fa fa-cog tx-24"></i>
                    <span class="menu-item-label">Paramètres</span>
                    <i class="menu-item-arrow fa fa-angle-down"></i>
                </div>
                <!-- menu-item -->
            </a>
            <!-- br-menu-link -->
            <ul class="br-menu-sub nav flex-column">
                <li class="nav-item"><a href="../utilisateur/mon_compte.php" class="nav-link">Mon compte</a></li>
                <!-- <li class="nav-item"><a href="form-layouts.html" class="nav-link">Creer un Service</a></li> -->
                <li class="nav-item"><a href="../gare/gare.php" class="nav-link">Création des gares</a></li>
                <li class="nav-item"><a href="../destination/destination.php" class="nav-link">Gestion des voyages</a></li>
                <li class="nav-item"><a href="../compagnie/compagnie.php" class="nav-link">Gestion des compagnies</a></li>
                <li class="nav-item"><a href="../affectation/affectation.php" class="nav-link">Gestion des affectations</a></li>
                <?php if($_SESSION['role'] == '3'){ ?>
                <li class="nav-item"><a href="../utilisateur/utilisateur.php" class="nav-link">Gestion des utilisateurs</a></li>
                <?php } ?>
                <?php if($_SESSION['role'] == '3'){ ?>
                <li class="nav-item"><a href="../annonce/annonce.php" class="nav-link">Gestion des annonces</a></li>
                <?php } ?>
            </ul>
            <?php if($_SESSION['role'] == '3'){ ?>
            <a href="../historique/historique.php" class="br-menu-link">
                <div class="br-menu-item">
                    <i class="fa fa-book tx-20"></i>
                    <span class="menu-item-label">Historiques</span>
                </div>
                <!-- menu-item -->
            </a>
            <?php } ?>
            <!-- br-menu-link -->
        </div>
        <!-- br-sideleft-menu -->

        <br>
    </div>
    <!-- br-sideleft -->
