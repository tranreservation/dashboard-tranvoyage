
<div class="br-header">
        <div class="br-header-left">
            <div class="navicon-left hidden-md-down"><a id="btnLeftMenu" href="#"><i class="fa fa-arrow-left"></i></a></div>
            <div class="navicon-left hidden-lg-up"><a id="btnLeftMenuMobile" href="#"><i class="fa fa-arrow-left"></i></a></div>
            <!-- input-group -->
        </div>
        <!-- br-header-left -->
        <div class="br-header-right">
            <nav class="nav">
                <div class="dropdown">
                    <a href="#" class="nav-link pd-x-7 pos-relative" data-toggle="dropdown">
                        <i class="fa fa-bell tx-24"></i>
                        <!-- start: if statement -->
                        <span class="square-8 bg-danger pos-absolute t-15 r-5 rounded-circle"></span>
                        <!-- end: if statement -->
                    </a>
                    <div class="dropdown-menu dropdown-menu-header wd-300 pd-0-force">
                        <div class="d-flex align-items-center justify-content-between pd-y-10 pd-x-20 bd-b bd-gray-200">
                            <label class="tx-12 tx-info tx-uppercase tx-semibold tx-spacing-2 mg-b-0">Notifications</label>
                            <a href="#" class="tx-11">Marquer comme lu</a>
                        </div>
                        <!-- d-flex -->

                        <div class="media-list">
                            <!-- loop starts here -->
                            <a href="#" class="media-list-link read">
                                <div class="media pd-x-20 pd-y-15">
                                    <img src="img/user-default.png" class="wd-40 rounded-circle" alt="">
                                    <div class="media-body">
                                        <p class="tx-13 mg-b-0 tx-gray-700"><strong class="tx-medium tx-gray-800">Suzzeth Bungaos</strong> tagged you and 18 others in a post.</p>
                                        <span class="tx-12">October 03, 2017 8:45am</span>
                                    </div>
                                </div>
                                <!-- media -->
                            </a>
                            <!-- loop ends here -->
                            <div class="pd-y-10 tx-center bd-t">
                                <a href="#" class="tx-12"><i class="fa fa-angle-down mg-r-5"></i> Voir toutes les Notifications</a>
                            </div>
                        </div>
                        <!-- media-list -->
                    </div>
                    <!-- dropdown-menu -->
                </div>
                <!-- dropdown -->
                <div class="dropdown">
                    <a href="#" class="nav-link nav-link-profile" data-toggle="dropdown">
                        <span class="logged-name hidden-md-down"><?php echo " ".$_SESSION['nom_prenoms']." "; ?></span>
                        <img src="../../img/user-default.png" class="wd-32 rounded-circle" alt="">
                        <span class="square-10 bg-success"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-header wd-200">
                        <ul class="list-unstyled user-profile-nav">
                            <li><a href="../utilisateur/mon_compte.php"><i class="fa fa-user"></i> Mon Compte</a></li>
                            <li><a href="../connexion/deconex.php"><i class="fa fa-power-off"></i> Deconnexion</a></li>
                        </ul>
                    </div>
                    <!-- dropdown-menu -->
                </div>
                <!-- dropdown -->
            </nav>

        </div>
        <!-- br-header-right -->
     </div>