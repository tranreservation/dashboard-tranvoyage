<section class="top_place section_padding">
        <div class="container">
            <!-- <div class="row justify-content-center">
                <div class="col-xl-6">
                    <div class="section_tittle text-center">
                        <h2>Aucun résultat trouvé <i class="fa fa-search fa-2x"></i></h2>
                    </div>
                </div>

            </div> -->

            <div class="row" style="margin-top:-15px;">
                <div class="col-md-12">
                    <div class="marquee-rtl">
                        <!-- le contenu défilant -->
                        <div>LA LISTE DES GARES SELON LES DESTINATIONS: Leopard Transport , plus besoin de vous déplacer si vous vous pouvez acheter vos tickets en toute sécutité ici</div>
                    </div>
                </div>
                <br><br>
                <div class="col-md-6">
                    <div class="gars_affiche">
                        <div class="row">
                            <div class="col-md-3" id="desktop1">
                                <i class="fa fa-bus fa-3x" style="padding: 10px;color:white;"></i>
                            </div>
                            <div class="col-md-3" id="mobile1">
                                <i class="fa fa-bus fa-3x" style="padding: 10px;color:white; "></i> Lundi 05 Avril 2021
                                <a href=" " class="btn " type="button " style="background-color:#229954;color: white;border:2px solid white;height:30px;margin-bottom:3px;border-radius:15px;font-size: 10px;font-weight: bold; ">Reserver le ticket</a>
                            </div>
                            <div class="col-md-9">
                                <div id="desktop2">
                                    <span style="font-family: Arial, Helvetica, sans-serif;font-size:1.2em;font-weight: bold;">
									      Lundi, 05 Avril 2021
								    </span>
                                    <a href="" class="btn " type="button " style="background-color:#229954;color: white;border:2px solid white;height:30px;margin-bottom:3px;border-radius:15px;font-size: 10px;font-weight: bold;margin-left:120px;; ">Reserver le ticket</a>
                                </div>
                                <div class="col-md-12" style="border-bottom:3px solid white; "></div>
                                <div class="col-md-12" style="background: #14d5ee;">
                                    <span style="font-family: Arial, Helvetica, sans-serif;font-weight: bold;margin-left:-0.90em; ">
									 <span style="font-size:1em;color: #1E8449; ">Abidjan - Bouaké | Départ: </span> <span style="font-size:1em;color:white; ">09h30 min</span>
                                    </span>
                                </div>
                                <div class="col-md-12" style="border-bottom:3px solid white; "></div>
                                <div class="col-md-12" style="background: #7DBF50;">
                                    <span style="font-family: Arial, Helvetica, sans-serif;font-weight: bold;margin-left:-0.90em; ">
									 <span style="font-size:12px;">Tarif:</span> <span style="font-size:12px;color:white;">5000 f.cfa</span> &nbsp;&nbsp;
                                    <span style="font-size:12px;"> Compagnie: </span><span style="font-size:12px;color:white;">Leopard transport</span>
                                    </span>
                                </div>
                                <div class="col-md-12" style="border-bottom:3px solid white; "></div>
                                <div class="col-md-12" style="background: #48C9B0;">
                                    <span style="font-family: Arial, Helvetica, sans-serif;font-weight: bold;margin-left:-0.90em; ">
                                    <span style="font-size:12px;">Gare: </span><span style="font-size:12px;color:white;">Leopard transport</span>
                                    <!-- <span style="font-size:12px;">Ouverture - Fermeture </span><span style="font-size:12px;color:white;">08H - 21H</span> -->
                                    </span>
                                </div>
                            </div>



                        </div>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="gars_affiche">
                        <div class="row">
                            <div class="col-md-3" id="desktop10">
                                <i class="fa fa-bus fa-3x" style="padding: 10px;color:white;"></i>
                            </div>
                            <div class="col-md-3" id="mobile10">
                                <i class="fa fa-bus fa-3x" style="padding: 10px;color:white; "></i> Lundi 05 Avril 2021
                                <a href=" " class="btn " type="button " style="background-color:#229954;color: white;border:2px solid white;height:30px;margin-bottom:3px;border-radius:15px;font-size: 10px;font-weight: bold; ">Reserver le ticket</a>
                            </div>
                            <div class="col-md-9">
                                <div id="desktop20">
                                    <span style="font-family: Arial, Helvetica, sans-serif;font-size:1.2em;font-weight: bold;">
									      Lundi, 05 Avril 2021
								    </span>
                                    <a href="reservation.php" class="btn " type="button " style="background-color:#229954;color: white;border:2px solid white;height:30px;margin-bottom:3px;border-radius:15px;font-size: 10px;font-weight: bold;margin-left:120px;; ">Reserver le ticket</a>
                                </div>
                                <div class="col-md-12" style="border-bottom:3px solid white; "></div>
                                <div class="col-md-12" style="background: #14d5ee;">
                                    <span style="font-family: Arial, Helvetica, sans-serif;font-weight: bold;margin-left:-0.90em; ">
									 <span style="font-size:1em;color: #1E8449; ">Abidjan - Bouaké | Départ: </span> <span style="font-size:1em;color:white; ">09h30 min</span>
                                    </span>
                                </div>
                                <div class="col-md-12" style="border-bottom:3px solid white; "></div>
                                <div class="col-md-12" style="background: #7DBF50;">
                                    <span style="font-family: Arial, Helvetica, sans-serif;font-weight: bold;margin-left:-0.90em; ">
									 <span style="font-size:12px;">Tarif:</span> <span style="font-size:12px;color:white;">5000 f.cfa</span> &nbsp;&nbsp;
                                    <span style="font-size:12px;"> Compagnie: </span><span style="font-size:12px;color:white;">Leopard transport</span>
                                    </span>
                                </div>
                                <div class="col-md-12" style="border-bottom:3px solid white; "></div>
                                <div class="col-md-12" style="background: #48C9B0;">
                                    <span style="font-family: Arial, Helvetica, sans-serif;font-weight: bold;margin-left:-0.90em; ">
                                    <span style="font-size:12px;">Gare: </span><span style="font-size:12px;color:white;">Leopard transport</span>
                                    <!-- <span style="font-size:12px;">Ouverture - Fermeture </span><span style="font-size:12px;color:white;">08H - 21H</span> -->
                                    </span>
                                </div>
                            </div>



                        </div>

                    </div>
                </div>
            </div>

        </div>
    </section>