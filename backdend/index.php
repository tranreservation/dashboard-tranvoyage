<?php 
    session_start();
    include('../server.php');
    if (!isset($_SESSION['pass_rh']) && $_SESSION['pass_rh']=='' && !isset($_SESSION['id_session_user']) && $_SESSION['id_session_user']=='' && $_SESSION['nom_prenoms']=='') {
     header("Location:./views/connexion/login.php");
}

?>
<!DOCTYPE html>
<html lang="fr">

<?php include "./template/head.php"; ?>
<body>

    <!-- Bracket header.php -->
    <!-- br-header sidebar.php-->
    <?php //include "./template/header.php"; ?>
    <div class="br-header">
        <div class="br-header-left">
            <div class="navicon-left hidden-md-down"><a id="btnLeftMenu" href="#"><i class="fa fa-arrow-left"></i></a></div>
            <div class="navicon-left hidden-lg-up"><a id="btnLeftMenuMobile" href="#"><i class="fa fa-arrow-left"></i></a></div>
            <!-- input-group -->
        </div>
        <!-- br-header-left -->
        <div class="br-header-right">
            <nav class="nav">
                <div class="dropdown">
                    <a href="#" class="nav-link pd-x-7 pos-relative" data-toggle="dropdown">
                        <i class="fa fa-bell tx-24"></i>
                        <!-- start: if statement -->
                        <span class="square-8 bg-danger pos-absolute t-15 r-5 rounded-circle"></span>
                        <!-- end: if statement -->
                    </a>
                    <div class="dropdown-menu dropdown-menu-header wd-300 pd-0-force">
                        <div class="d-flex align-items-center justify-content-between pd-y-10 pd-x-20 bd-b bd-gray-200">
                            <label class="tx-12 tx-info tx-uppercase tx-semibold tx-spacing-2 mg-b-0">Notifications</label>
                            <a href="#" class="tx-11">Marquer comme lu</a>
                        </div>
                        <!-- d-flex -->

                        <div class="media-list">
                            <!-- loop starts here -->
                            <a href="#" class="media-list-link read">
                                <div class="media pd-x-20 pd-y-15">
                                    <img src="img/user-default.png" class="wd-40 rounded-circle" alt="">
                                    <div class="media-body">
                                        <p class="tx-13 mg-b-0 tx-gray-700"><strong class="tx-medium tx-gray-800">Suzzeth Bungaos</strong> tagged you and 18 others in a post.</p>
                                        <span class="tx-12">October 03, 2017 8:45am</span>
                                    </div>
                                </div>
                                <!-- media -->
                            </a>
                            <!-- loop ends here -->
                            <div class="pd-y-10 tx-center bd-t">
                                <a href="#" class="tx-12"><i class="fa fa-angle-down mg-r-5"></i> Voir toutes les Notifications</a>
                            </div>
                        </div>
                        <!-- media-list -->
                    </div>
                    <!-- dropdown-menu -->
                </div>
                <!-- dropdown -->
                <div class="dropdown">
                    <a href="#" class="nav-link nav-link-profile" data-toggle="dropdown">
                        <span class="logged-name hidden-md-down"><?php echo " ".$_SESSION['nom_prenoms']." "; ?></span>
                        <img src="img/user-default.png" class="wd-32 rounded-circle" alt="">
                        <span class="square-10 bg-success"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-header wd-200">
                        <ul class="list-unstyled user-profile-nav">
                            <li><a href="./views/utilisateur/mon_compte.php"><i class="fa fa-user"></i> Mon Compte</a></li>
                            <li><a href="./views/connexion/deconex.php"><i class="fa fa-power-off"></i> Deconnexion</a></li>
                        </ul>
                    </div>
                    <!-- dropdown-menu -->
                </div>
                <!-- dropdown -->
            </nav>

        </div>
        <!-- br-header-right -->
     </div>
    <?php  //include "./template/sidebar.php"; ?>
    <div class="br-logo"><a href="../../index.php"><span>[</span>Tranvoyage<span>]</span></a></div>
    <div class="br-sideleft overflow-y-auto">
        <label class="sidebar-label pd-x-15 mg-t-20">Navigation</label>
        <div class="br-sideleft-menu">
            <a href="../../index.php" class="br-menu-link active">
                <div class="br-menu-item">
                    <i class="fa fa-home tx-24"></i>
                    <span class="menu-item-label">Tableau de bord</span>
                </div>
                <!-- menu-item -->
            </a>
            <!-- br-menu-link -->
            <a href="#" class="br-menu-link">
                <div class="br-menu-item">
                    <i class="fa fa-envelope tx-24"></i>
                    <span class="menu-item-label">Notification</span>
                </div>
                <!-- menu-item -->
            </a>
            <!-- br-menu-link -->

            <a href="#" class="br-menu-link">
                <div class="br-menu-item">
                    <i class="fa fa-bus tx-24"></i>
                    <span class="menu-item-label">Espace gares</span>
                    <i class="menu-item-arrow fa fa-angle-down"></i>
                </div>
                <!-- menu-item -->
            </a>
            <!-- br-menu-link -->
            <ul class="br-menu-sub nav flex-column">
                <!-- <li class="nav-item"><a href="#" class="nav-link">Validation des Tickets</a></li> -->
                <!-- <li class="nav-item"><a href="#" class="nav-link">Les Services</a></li> -->
                <li class="nav-item"><a href="./views/destination/liste_voyage.php" class="nav-link">Liste des voyages</a></li>
            </ul>
            <?php if($_SESSION['role'] == '3'){ ?>
            <a href="./views/destination/paiement.php" class="br-menu-link">
                <div class="br-menu-item">
                    <i class="fas fa-ticket-alt tx-22"></i>
                    <span class="menu-item-label">Gestion des tickets</span>
                </div>
                <!-- menu-item -->
            </a>
            <?php } ?>
            <!-- br-menu-link -->
            <a href="#" class="br-menu-link">
                <div class="br-menu-item">
                    <i class="fa fa-cog tx-24"></i>
                    <span class="menu-item-label">Paramètres</span>
                    <i class="menu-item-arrow fa fa-angle-down"></i>
                </div>
                <!-- menu-item -->
            </a>
            <!-- br-menu-link -->
            <ul class="br-menu-sub nav flex-column">
                <li class="nav-item"><a href="./views/utilisateur/mon_compte.php" class="nav-link">Mon compte</a></li>
                <li class="nav-item"><a href="./views/destination/destination.php" class="nav-link">Création des gares</a></li>
                <li class="nav-item"><a href="./views/destination/destination.php" class="nav-link">Gestion des voyages</a></li>
                <li class="nav-item"><a href="./views/compagnie/compagnie.php" class="nav-link">Gestion des compagnies</a></li>
                <li class="nav-item"><a href="./views/affectation/affectation.php" class="nav-link">Gestion des affectations</a></li>
                <?php if($_SESSION['role'] == '3'){ ?>
                <li class="nav-item"><a href="./views/utilisateur/utilisateur.php" class="nav-link">Gestion des utilisateurs</a></li>
                <?php } ?>
                <?php if($_SESSION['role'] == '3'){ ?>
                <li class="nav-item"><a href="./views/annonce/annonce.php" class="nav-link">Gestion des annonces</a></li>
                <?php } ?>
            </ul>
            <?php if($_SESSION['role'] == '3'){ ?>
            <a href="./views/historique/historique.php" class="br-menu-link">
                <div class="br-menu-item">
                    <i class="fa fa-book tx-20"></i>
                    <span class="menu-item-label">Historiques</span>
                </div>
                <!-- menu-item -->
            </a>
            <?php } ?>
            <!-- br-menu-link -->
        </div>
        <!-- br-sideleft-menu -->

        <br>
    </div>
    <!-- br-sideleft -->


    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
        <div class="pd-30">
            <h4 class="tx-gray-800 mg-b-5">Tableau de bord</h4>
            <p class="mg-b-0"></p>
        </div>
        <!-- d-flex -->

        <div class="br-pagebody mg-t-5 pd-x-30">
   
        <?php include "./views/dashboard.php"; ?>

        </div>
        <!-- ########## FOOTER ########## -->
        <?php include "./template/footer.php"; ?>
          <!-- ########## END: FOOTER ########## footer.js -->
    </div>
    <!-- br-mainpanel -->
    <!-- ########## END: MAIN PANEL ########## script.js-->
    <?php include "./template/scripts.php"; ?>

</body>

<!-- Mirrored from themepixels.me/demo/bracket/app/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 01 Sep 2020 17:52:50 GMT -->

<!-- Mirrored from localhost/tran/public/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 21 Mar 2021 23:43:54 GMT -->

</html>