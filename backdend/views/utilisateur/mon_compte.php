 
<?php
   session_start();
   include('../../../server.php');

  if (!isset($_SESSION['pass_rh']) && $_SESSION['pass_rh']=='' && !isset($_SESSION['id_session_user']) && $_SESSION['id_session_user']=='') {
    header("Location:../connexion/login.php");
}


?>
    <!DOCTYPE html>
<html lang="fr">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<!-- /Added by HTTrack -->

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Bracket">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="bracket/img/bracket-social.html">

    <!-- Facebook -->
    <meta property="og:url" content="">
    <meta property="og:title" content="Bracket">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="bracket/img/bracket-social.html">
    <meta property="og:image:secure_url" content="bracket/img/bracket-social.html">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>Tranvoyage</title>
    <link href="../../fontawesome-free-web/css/all.css" rel="stylesheet">
    <script defer src="../../fontawesome-free-web/js/all.js"></script>
    <link href="../../fontawesome-free-web/css/fontawesome.css" rel="stylesheet">
    <link href="../../fontawesome-free-web/css/brands.css" rel="stylesheet">
    <link href="../../fontawesome-free-web/css/solid.css" rel="stylesheet">
    <script defer src="../../fontawesome-free-web/js/brands.js"></script>
    <script defer src="../../fontawesome-free-web/js/solid.js"></script>
    <script defer src="../../fontawesome-free-web/js/fontawesome.js"></script>

    <link href="../../lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="../../lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
    <link href="../../lib/jquery-switchbutton/jquery.switchButton.css" rel="stylesheet">
    <link href="../../lib/rickshaw/rickshaw.min.css" rel="stylesheet">
    <link href="../../lib/chartist/chartist.css" rel="stylesheet">
    <link href="../../lib/datatables/jquery.dataTables.css" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="../../css/bracket.css">
    <link rel="stylesheet" href="style.css">
     <!-- toast CSS -->
    <link  href="../../lib/toastr/toast.style.css" rel="stylesheet">
    <link rel="stylesheet" href="../../lib/sweetalert2/sweetalert2.min.css">
    <style>
    .error{
        outline: 1px solid red;
    }    
    
   </style>
</head>
<!-- <link rel="stylesheet" href="style.css"> -->
<?php include "../../template/header.php"; ?>
<?php include "../../template/sidebar.php"; ?>
<style>
    #docscard:hover{
     font-size: 16px;
    }
</style>
<div class="br-mainpanel">
      <div class="br-pageheader pd-y-15 pd-l-20">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="#">Tranvoyage</a>
          <a class="breadcrumb-item" href="#">Profil</a>
          <span class="breadcrumb-item active">Compte utilisateur</span>
        </nav>
      </div><!-- br-pageheader -->
    
   
    <div class="br-pagebody">
    <div class="br-section-wrapper">   
    <h6 class="tx-uppercase tx-gray-800 tx-uppercase tx-bold tx-14 mg-b-10"><i class="fa fa-edit"></i> Modification du mot de passe</h6>
          <!-- <p class="mg-b-30 tx-gray-600">A demo for using custom layout for validation.</p> -->
          <div style="height:20px">
            <div class="div_load"><img src="../../../img/progressbar.gif" alt="image chargement" /></div>
            <div class="msg_erreur"></div> 
            <div class="msg_ok"></div>  
          </div> 
							  
          <form action="#" id="form_mod" data-parsley-validate>
            <div class="form-layout form-layout-2">
              <div class="row no-gutters">
                <div class="col-md-4">
                  <div class="form-group">
                    <label class="form-control-label">Mot de passe actuel: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="password" id="mot_actuel" name="mot_actuel" placeholder="Entrer le mot de passe" required>
                  </div>
                </div><!-- col-4 -->
                <div class="col-md-4 mg-t--1 mg-md-t-0">
                  <div class="form-group mg-md-l--1">
                    <label class="form-control-label">Nouveau mot de passe: <span class="tx-danger">*</span></label>
                    <input class="form-control" id="new_mot" name="new_mot"  type="password" name="lastname" placeholder="Entrer le nouveau mot de passe" required>
                  </div>
                </div><!-- col-4 -->
                <div class="col-md-4 mg-t--1 mg-md-t-0">
                  <div class="form-group mg-md-l--1">
                    <label class="form-control-label">Confirmer mot de passe: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="password"id="conf_mot" name="conf_mot" placeholder="Confirmer" required>
                  </div>
                </div><!-- col-4 -->
              </div><!-- row -->
              <div class="form-layout-footer bd pd-20 bd-t-0">
                <button class="btn btn-info" type="submit"><i class="fas fa-save"></i> Enregistrer</button>&nbsp;&nbsp;&nbsp;
                <button class="btn btn-danger" type="reset"> <i class="fa fa-times"></i> Annuler</button>
              </div><!-- form-group -->
            </div><!-- form-layout -->
          </form>

          <!-- <p class="tx-11 tx-uppercase tx-spacing-2 mg-t-40 mg-b-10 tx-gray-600">Class Reference</p>
          <table class="table table-bordered tx-13 tx-gray-700 bd">
            <thead>
              <tr class="bg-gray-100 tx-11 tx-uppercase tx-gray-800">
                <th class="wd-100p">Description</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>To have a custom styled error message, make sure you wrapped the input wit this class. See code above for reference.</td>
              </tr>
            </tbody>
          </table> -->
     
          </div><!-- br-section-wrapper -->
      </div><!-- br-pagebody -->
    <?php include "../../template/footer.php"; ?>

</div>

<script src="../../lib/jquery/jquery.js"></script>
    <script src="../../lib/popper.js/popper.js"></script>
    <script src="../../lib/bootstrap/bootstrap.js"></script>
    <script src="../../lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
    <script src="../../lib/moment/moment.js"></script>
    <script src="../../lib/jquery-ui/jquery-ui.js"></script>
    <script src="../../lib/jquery-switchbutton/jquery.switchButton.js"></script>
    <script src="../../lib/peity/jquery.peity.js"></script>
    <script src="../../lib/chartist/chartist.js"></script>
    <script src="../../lib/jquery.sparkline.bower/jquery.sparkline.min.js"></script>
    <script src="../../lib/d3/d3.js"></script>
    <script src="../../lib/rickshaw/rickshaw.min.js"></script>


    <script src="../../js/bracket.js"></script>
    <script src="../../js/ResizeSensor.js"></script>
    <script src="../../js/dashboard.js"></script>
    <script src="../../lib/datatables/jquery.dataTables.js"></script>
    <script src="../../lib/datatables-responsive/dataTables.responsive.js"></script>
    <script src="../../lib/toastr/toast.script.js"></script>
    <script src="../../lib/sweetalert2/sweetalert2.min.js"></script>
    <!-- <script src="../../lib/js/function_utilisateur.js"></script> -->
    <!-- <script src="../../lib/js/co_form.js"></script> -->
   <script>
           ///////////////UTILIDSATEUR MODIFICATION MOTS DE PASSE///////////
           
    $(".div_load").hide();
    $(".msg_erreur").hide();
    $(".msg_ok").hide();

    $('#form_mod').on('submit', function() {
       // alert();
if ($("#new_mot").val() != $("#conf_mot").val()) {
    $("div.msg_erreur").show();
    $("div.msg_erreur").addClass("red");
    $("div.msg_erreur").removeClass("green");
    $("div.msg_erreur").html("Les nouveaux mots de passe ne sont pas identiques !").show();
    $("#conf_mot").focus();
    var title = "Erreur",
    message = "Les nouveaux mots de passe ne sont pas identiques !",
    type = "error";
    addtoast(title, message, type);
    setTimeout(function() { $("div.msg_erreur").hide(); }, 3500);
} else {
    $("div.msg_erreur").hide();
    $.ajax({
        type: "POST",
        url: "modif_passe.php",
        data: "mot_actuel=" + $("#mot_actuel").val() + "&new_mot=" + $("#new_mot").val() + "&conf_mot=" + $("#conf_mot").val(),
        success: function(msg) {

            if (msg == 1) {
                $("div.msg_erreur").show();
                $("div.msg_erreur").addClass("red");
                $("div.msg_erreur").removeClass("green");
                $("div.msg_erreur").html("Le mot de passe actuel n'est pas valide !").show();
                var title = "Erreur",
                message = "Le mot de passe actuel n'est pas valide !",
                type = "error";
                addtoast(title, message, type);
                //$("div.div_load").show();
                setTimeout(function() {
                    $("div.msg_erreur").hide();
                    $("#mot_actuel").focus();
                }, 3500);
            } else {
                $("div.div_load").show();
                setTimeout(function() {
                    var title = "Modification Mot de passe",
                    message = "Mot de passe modifié avec succès",
                    type = "success";
                Swal.fire({
                    position: 'top-end',
                    icon: type,
                    title: message,
                    showConfirmButton: false,
                    timer: 1500
                })
                    $("#mot_actuel").focus();
                    $("div.div_load").hide();
                    $("#mot_actuel").val('');
                    $("#new_mot").val('');
                    $("#conf_mot").val('');
                }, 3500);

                setTimeout(function() {
                    $("div.msg_ok").hide();
                }, 6500);
                setTimeout(function() {
                    $(location).attr("href", "../connexion/login.php");
                }, 6500);


            }

        }
    });
}

return false;
});


function addtoast(title, message, type) {
    $.Toast(title, message, type, {
        has_icon: true,
        has_close_btn: true,
        stack: true,
        fullscreen: true,
        timeout: 8000,
        sticky: false,
        has_progress: true,
        rtl: false,
    });
    //location.reload();
}
/////////////////////
   </script>
    </body>

<!-- Mirrored from themepixels.me/demo/bracket/app/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 01 Sep 2020 17:52:50 GMT -->

<!-- Mirrored from localhost/tran/public/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 21 Mar 2021 23:43:54 GMT -->

</html>
  
