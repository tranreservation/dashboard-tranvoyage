<?php

session_start();
include('../../../server.php');

$id = $_GET["ref_user"];


$con = Server::connexion();

$con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

if($id!='')
{
$red=$con->prepare("SELECT * FROM utilisateurs WHERE id_utilisateurs=:A"); 
$red->execute(array('A'=>$id));
$util=$red->fetch();
$etat = $util['status'];

$stit_st='Supprimer';

$_SESSION['id_utilisateur_mod']=$util["id_utilisateurs"];
//}
?>

<form action="#"  enctype="multipart/form-data" id="form_modif_utilisateur">
                <div class="modal-body pd-20">
                <div class="row">
                              <div class="col-md-3">
                                <div class="form-group">
                                <label for="">Nom <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" name="nom_modif" id="nom_modif" value="<?php echo $util['nom'] ?>" required>
                                </div>
                              </div>
                              
                              <div class="col-md-5">
                              <div class="form-group">
                                <label for="">Prénoms <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" name="prenoms_modif" id="prenoms_modif" value="<?php echo $util['prenoms'] ?>" required>
                                </div>
                              </div>
                              <div class="col-md-4">
                              <div class="form-group">
                                <label for="">Téléphone <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" name="telephone_modif" id="telephone_modif" value="<?php echo $util['telephone'] ?>" required>
                                </div>
                              </div>
                              <div class="col-md-4">
                              <div class="form-group">
                                <label for="">Email <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" name="email_modif" id="email_modif" value="<?php echo $util['email'] ?>" required>
                                </div>
                              </div>
                              <div class="col-md-8">
                              <div class="form-group">
                                <label for="">Type utilisateur <span style="color: red;">*</span></label>
                                <select name="type_utilisateur_modif" id="type_utilisateur_modif" class="form-control" required>
                                    <option value="" selected>---choisir---</option>
                                     <option value="0" <?php if ($util['type_utilisateurs_id']=='0') { echo 'selected'; } ?>>Client</option>
                                     <option value="1"  <?php if ($util['type_utilisateurs_id']=='1') { echo 'selected'; } ?>>Partenaire</option>
                                     <option value="2"  <?php if ($util['type_utilisateurs_id']=='2') { echo 'selected'; } ?>>Administrateur</option>
                                     <option value="3"  <?php if ($util['type_utilisateurs_id']=='3') { echo 'selected'; } ?>>Super administrateur</option>
                                </select>
                                </div>
                              </div>
                              <div class="col-md-8" id="aff_compaganie_mod">
                              <div class="form-group">
                                <label for="">Compagnie <span style="color: red;">*</span> </label>
                                <select name="compagnie_modif" id="compagnie_modif" class="form-control">
                                     <option value="">---choisir---</option>
                                <?php $query="SELECT * FROM compagnie WHERE id_compagnie!='0'"; $compagne = Server::Allquery($query,$con); 
                                foreach ($compagne as $comp) {  ?>
                                     <option value="<?php echo $comp['id_compagnie'] ?>" <?php if ($util['compagnie_id']==$comp['id_compagnie']) { echo 'selected'; } ?>><?php echo $comp['nom_compagnie'] ?></option>
                                <?php } ?>
                                </select>
                                </div>
                              </div>
                         
                         </div>     
                         
                </div><!-- modal-body -->
                <div class="modal-footer">
                  <button type="submit" id="btn_modif_utilisateur" class="btn btn-primary tx-size-xs"><i class="fas fa-save"></i> Modifier</button>
                  <button type="button" class="btn btn-danger tx-size-xs" data-dismiss="modal"><i class="fas fa-times"></i> Fermer</button>
                </div>
              </form> 



    <script>
        $('#form_modif_utilisateur').on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            cache: false,
            contentType: false,
            processData: false,
            url: "mod_utilisateur.php",
            data: new FormData(this),
            success: function(msg) {
             //alert(msg);
                if (msg == 1) {
                //  alert(msg);
                  var title="Erreur", message="Cet utilisateur existe déjà", type="error";
                    addtoast(title, message, type);
                }else if (msg == 2) {
                    var title="Erreur un champs est vide", message="Veuillez remplir le champs vide", type="error";
                   addtoast(title, message, type);
                } else {
                   // alert(msg);
                    $("div.msg_erreur").hide();
                    $('#myModal_utilisateur_mod').modal('toggle');
                    var title="Modification utilisateur", message="Utilisateur Modifié avec succès", type="success";
                    Swal.fire({
                    position: 'top-end',
                    icon: type,
                    title: message,
                    showConfirmButton: false,
                    timer: 1500
                  })
                  
                  setTimeout(function() {
                       location.reload();
                    }, 2500)
                    
                   // addtoast(title, message, type);

                }
            }
        });

        return false;

    });


    function addtoast(title, message, type){
$.Toast(title, message, type, {
                    has_icon:true,
                    has_close_btn:true,
					stack: true,
                    fullscreen:true,
                    timeout:8000,
                    sticky:false,
                    has_progress:true,
                    rtl:false,
                });

            }
    //fin modifier
</script>
    
<?php } ?>              

 