<?php

session_start();
include('../../../server.php');

$id = $_GET["ref_comp"];


$con = Server::connexion();

$con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

if($id!='')
{
$red=$con->prepare("SELECT * FROM compagnie WHERE id_compagnie=:A"); 
$red->execute(array('A'=>$id));
$util=$red->fetch();


$stit_st='Supprimer';

$_SESSION['id_compagnie_mod']=$util["id_compagnie"];
//}
?>

<form action="#"  enctype="multipart/form-data" id="form_modif_compagnie">
                <div class="modal-body pd-20">
                <div class="row">
                              <div class="col-md-3">
                                <div class="form-group">
                                <label for="">Nom de la compagnie<span style="color: red;">*</span></label>
                                <input type="text" class="form-control" name="nom_compagnie_modif" id="nom_compagnie_modif" value="<?php echo $util['nom_compagnie'] ?>" required>
                                </div>
                              </div>
                              <div class="col-md-4">
                              <div class="form-group">
                                <label for="">Téléphone <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" name="telephone_comp_modif" id="telephone_comp_modif" value="<?php echo $util['telephone_comp'] ?>" required>
                                </div>
                              </div>
                              <div class="col-md-4">
                              <div class="form-group">
                                <label for="">Email compagnie <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" name="email_comp_modif" id="email_comp_modif" value="<?php echo $util['email_comp'] ?>" required>
                                </div>
                              </div>
                         
                         </div>     
                         
                </div><!-- modal-body -->
                <div class="modal-footer">
                  <button type="submit" id="btn_modif_compagnie" class="btn btn-primary tx-size-xs"><i class="fas fa-save"></i> Modifier</button>
                  <button type="button" class="btn btn-danger tx-size-xs" data-dismiss="modal"><i class="fas fa-times"></i> Fermer</button>
                </div>
              </form> 



    <script>
        $('#form_modif_compagnie').on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            cache: false,
            contentType: false,
            processData: false,
            url: "mod_compagnie.php",
            data: new FormData(this),
            success: function(msg) {
             //alert(msg);
                if (msg == 1) {
                //  alert(msg);
                  var title="Erreur", message="Cette compagnie existe déjà", type="error";
                    addtoast(title, message, type);
                }else if (msg == 2) {
                    var title="Erreur un champs est vide", message="Veuillez remplir le champs vide", type="error";
                   addtoast(title, message, type);
                } else {
                   // alert(msg);
                    $("div.msg_erreur").hide();
                    $('#myModal_compagnie_mod').modal('toggle');
                    var title="Modification compagnie", message="Compagnie modifié avec succès", type="success";
                    Swal.fire({
                    position: 'top-end',
                    icon: type,
                    title: message,
                    showConfirmButton: false,
                    timer: 1500
                  })
                  
                  setTimeout(function() {
                       location.reload();
                    }, 2500)
                    
                   // addtoast(title, message, type);

                }
            }
        });

        return false;

    });


    function addtoast(title, message, type){
$.Toast(title, message, type, {
                    has_icon:true,
                    has_close_btn:true,
					stack: true,
                    fullscreen:true,
                    timeout:8000,
                    sticky:false,
                    has_progress:true,
                    rtl:false,
                });

            }
    //fin modifier
</script>
    
<?php } ?>              

 