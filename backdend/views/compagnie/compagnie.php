<?php 
    session_start();
    include('../../../server.php');
    if (!isset($_SESSION['pass_rh']) && $_SESSION['pass_rh']=='' && !isset($_SESSION['id_session_Comp']) && $_SESSION['id_session_Comp']=='') {
     header("Location:../connexion/login.php");
}

    ?>
    <!DOCTYPE html>
<html lang="fr">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<!-- /Added by HTTrack -->

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Bracket">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="bracket/img/bracket-social.html">

    <!-- Facebook -->
    <meta property="og:url" content="">
    <meta property="og:title" content="Bracket">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="bracket/img/bracket-social.html">
    <meta property="og:image:secure_url" content="bracket/img/bracket-social.html">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>Tranvoyage</title>

    <!-- vendor css 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
    <link href="../../fontawesome-free-web/css/all.css" rel="stylesheet">
    <script defer src="../../fontawesome-free-web/js/all.js"></script>
    <link href="../../fontawesome-free-web/css/fontawesome.css" rel="stylesheet">
    <link href="../../fontawesome-free-web/css/brands.css" rel="stylesheet">
    <link href="../../fontawesome-free-web/css/solid.css" rel="stylesheet">
    <script defer src="../../fontawesome-free-web/js/brands.js"></script>
    <script defer src="../../fontawesome-free-web/js/solid.js"></script>
    <script defer src="../../fontawesome-free-web/js/fontawesome.js"></script>

    <link href="../../lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="../../lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
    <link href="../../lib/jquery-switchbutton/jquery.switchButton.css" rel="stylesheet">
    <link href="../../lib/rickshaw/rickshaw.min.css" rel="stylesheet">
    <link href="../../lib/chartist/chartist.css" rel="stylesheet">
    <link href="../../lib/datatables/jquery.dataTables.css" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="../../css/bracket.css">
     <!-- toast CSS -->
    <link  href="../../lib/toastr/toast.style.css" rel="stylesheet">
    <link rel="stylesheet" href="../../lib/sweetalert2/sweetalert2.min.css">
    <style>
    .error{
        outline: 1px solid red;
    }    
   </style>
</head>
<body>

    <!-- Bracket header.php -->
    <!-- br-header sidebar.php-->
    <?php include "../../template/header.php"; ?>
    <?php include "../../template/sidebar.php"; ?>

    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader pd-y-15 pd-l-20">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="#">Tranvoyage</a>
          <a class="breadcrumb-item" href="#">Compagnies</a>
          <span class="breadcrumb-item active">Gestion des compagnies</span>
        </nav>
      </div><!-- br-pageheader -->
      <!-- <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <h4 class="tx-gray-800 mg-b-5">Data Table</h4>
        <p class="mg-b-0">DataTables is a plug-in for the jQuery Javascript library.</p>
      </div> -->

      <div class="br-pagebody">
        <div class="br-section-wrapper">

          <h6 class="tx-gray-800 tx-uppercase tx-bold tx-14 mg-b-10" >Gestion des compagnies <i class="fa fa-university"></i></h6><br>
          <button type="button" class="btn" style="width:200px;color:#fff;background:#2471A3;cursor:pointer;" data-toggle="modal" data-target="#ModalajoutComp">Ajouter une compagnie  <i class="fas fa-plus-circle"></i></button>
          <br>
          <div  style="margin-bottom:10px;"></div>
          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-5p" style="text-align: center;">N</th>
                  <th class="wd-15p" style="text-align: center;">Nom de la compagnie</th>
                  <th class="wd-15p" style="text-align: center;">Email</th>
                  <th class="wd-10p" style="text-align: center;">Téléphone</th>
                  <th class="wd-10p" style="text-align: center;">Créé le</th>
                  <th class="wd-20p" style="text-align: center;">Actions</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                $requete = "SELECT * FROM compagnie WHERE id_compagnie!='0' ";
                $con = Server::connexion(); 
                $compagnie = Server::Allquery($requete, $con);
                //$con->query($requete)->fetchAll(); type_compagnie_id
                $compte_compagnie = $con->query($requete);
                $compteComp = $compte_compagnie->rowCount();
                $i=1;
                if ($compteComp>0) {

                foreach ($compagnie as $util) {
                 
                  $date = date("d/m/Y à H:i:s", strtotime($util['created_at']));
                 
                  $colors= "";

                ?>
                <tr>
                  <td style="text-align: center;background:<?php echo $colors; ?>"> <?php echo $i; ?></td>
                  <td style="text-align: center;background:<?php echo $colors; ?>"><?php echo $util['nom_compagnie']; ?></td>
                  <td style="text-align: center;background:<?php echo $colors; ?>"><?php echo $util['email_comp']; ?></td>
                  <td style="text-align: center;background:<?php echo $colors; ?>"><?php echo $util['telephone_comp']; ?></td>
                  <td style="text-align: center;background:<?php echo $colors; ?>"><?php echo $date; ?></td>
                  <td style="text-align: center;background:<?php echo $colors; ?>">
                    <a class="btn btn-sm btn-success modComp" data-toggle="modal" data-target="#myModal_compagnie_mod" href="#" data-id="<?php echo $util['id_compagnie']?>"><i class="fa fa-pencil-alt"></i></a>
                    <a class="btn btn-sm btn-danger supComp" data-toggle="modal" data-target="#myModal_compagnie_sup" data-id="<?php echo $util['id_compagnie']?>" href="" ><i class="fa fa-trash-alt"></i></a>
                  </td>
                </tr>
                <?php 
                $i++; }   
                
              } else {
                echo '';
              }
                ?>

              </tbody>
            </table>
          </div><!-- table-wrapper -->

           <!--les modals--->
                  <!-- AJOUT compagnie MODAL -->
                  <div id="ModalajoutComp" class="modal fade">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content tx-size-sm">
                <div class="modal-header pd-x-20">
                  <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="fas fa-plus-circle"></i> Ajouter une compagnie</h6>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body pd-20">
                <form action="#"  enctype="multipart/form-data" id="form_compagnie">
                <div class="row">
                              <div class="col-md-4">
                                <div class="form-group">
                                <label for="">Nom de la compagnie<span style="color: red;">*</span></label>
                                <input type="text" class="form-control" name="nom_compagnie" id="nom" required>
                                </div>
                              </div>
                              
                              <div class="col-md-4">
                              <div class="form-group">
                                <label for="">Téléphone <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" name="telephone_comp" id="telephone" required>
                                </div>
                              </div>
                              <div class="col-md-4">
                              <div class="form-group">
                                <label for="">Email <span style="color: red;">*</span></label>
                                <input type="email" class="form-control" name="email_comp" id="email_comp" required>
                                </div>
                              </div>

                         
                         </div>     
                         
                </div><!-- modal-body -->
                <div class="modal-footer">
                  <button type="submit" id="btn_ajout_compagnie" class="btn btn-success tx-size-xs"><i class="fas fa-save"></i> Enregistrer</button>
                  <button type="button" class="btn btn-danger tx-size-xs" data-dismiss="modal"><i class="fas fa-times"></i> Fermer</button>
                </div>
              </form>   
              </div>
            </div><!-- modal-dialog -->
          </div><!-- modal -->

                    <!-- MODAL ALERT MESSAGE -->
                    <div id="ajout-Comp-succes" class="modal fade">
            <div class="modal-dialog" role="document">
              <div class="modal-content tx-size-sm">
                <div class="modal-body tx-center pd-y-20 pd-x-20">
                  <button type="button" class="close" data-dismiss="modal"  aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <i class="icon ion-ios-checkmark-outline tx-100 tx-success lh-1 mg-t-20 d-inline-block"></i>
                  <i class="fas fa-check"></i>
                  <h4 class="tx-success tx-semibold mg-b-20">Félicitation!</h4>
                  <p class="mg-b-20 mg-x-20">compagnie ajouter avec succès.</p>
                  <button type="button" class="close" data-dismiss="modal" class="btn btn-success tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium mg-b-20">
                    Continuer</button>
                  </div><!-- modal-body -->
                </div><!-- modal-content -->
              </div><!-- modal-dialog -->
            </div><!-- modal -->
          <!--FIN AJOUT compagnie MODAL -->


<!-- -----Modal Status compagnie------ -->
          <!-- SMALL MODAL -->
          <div id="myModal_compagnie_etat" class="modal fade">
            <div class="modal-dialog modal-sm" role="document">
              <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-x-20">
                  <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Verrouillage</h6>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body pd-20 aff_status">
                  
                </div>
                <div class="modal-footer justify-content-center">
                  <button type="button" id="btn_status" class="btn btn-primary tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium">Oui</button>
                  <button type="button" class="btn btn-danger tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal">Non</button>
                </div>
              </div>
            </div><!-- modal-dialog -->
          </div><!-- modal -->

          <!-- -----Modal supprimer compagnie------ -->
          <!-- SMALL MODAL -->
          <div id="myModal_compagnie_sup" class="modal fade">
            <div class="modal-dialog modal-sm" role="document">
              <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-x-20">
                  <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Supprimer</h6>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body pd-20 aff_sup">
                 
                </div>
                <div class="modal-footer justify-content-center">
                  <button type="button" id="btn_sup" class="btn btn-primary tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium">Oui</button>
                  <button type="button" class="btn btn-danger tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal">Non</button>
                </div>
              </div>
            </div><!-- modal-dialog -->
          </div><!-- modal -->


             <!-- MODIF compagnie MODAL -->
             <div id="myModal_compagnie_mod" class="modal fade">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content tx-size-sm">
                <div class="modal-header pd-x-20">
                  <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="fas fa-plus-circle"></i> Modifier un compagnie</h6>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
               <div class="aff_mod"></div> 
              </div>
            </div><!-- modal-dialog -->
          </div><!-- modal -->

                     <!--fin des modals--->

        </div><!-- br-section-wrapper -->
      </div><!-- br-pagebody -->
      <?php include "../../template/footer.php"; ?>
   
    </div><!-- br-mainpanel toast.script.js-->

    
    <script src="../../lib/jquery/jquery.js"></script>
    <script src="../../lib/popper.js/popper.js"></script>
    <script src="../../lib/bootstrap/bootstrap.js"></script>
    <script src="../../lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
    <script src="../../lib/moment/moment.js"></script>
    <script src="../../lib/jquery-ui/jquery-ui.js"></script>
    <script src="../../lib/jquery-switchbutton/jquery.switchButton.js"></script>
    <script src="../../lib/peity/jquery.peity.js"></script>
    <script src="../../lib/chartist/chartist.js"></script>
    <script src="../../lib/jquery.sparkline.bower/jquery.sparkline.min.js"></script>
    <script src="../../lib/d3/d3.js"></script>
    <script src="../../lib/rickshaw/rickshaw.min.js"></script>


    <script src="../../js/bracket.js"></script>
    <script src="../../js/ResizeSensor.js"></script>
    <script src="../../js/dashboard.js"></script>
    <script src="../../lib/datatables/jquery.dataTables.js"></script>
    <script src="../../lib/datatables-responsive/dataTables.responsive.js"></script>
    <script src="../../lib/toastr/toast.script.js"></script>
    <script src="../../lib/sweetalert2/sweetalert2.min.js"></script>
    <!-- <script src="../../lib/js/function_compagnie.js"></script> -->
    <script>
    $(function() {
    'Compstrict'
     
    // FOR DEMO ONLY 
    // menu collapsed by default during first page load or refresh with screen
    // having a size between 992px and 1299px. This is intended on this page only
    // for better viewing of widgets demo.
    $(window).resize(function() {
        minimizeMenu();
    });

    minimizeMenu();

    function minimizeMenu() {
        if (window.matchMedia('(min-width: 992px)').matches && window.matchMedia('(max-width: 1299px)').matches) {
            // show only the icons and hide left menu label by default
            $('.menu-item-label,.menu-item-arrow').addClass('op-lg-0-force d-lg-none');
            $('body').addClass('collapsed-menu');
            $('.show-sub + .br-menu-sub').slideUp();
        } else if (window.matchMedia('(min-width: 1300px)').matches && !$('body').hasClass('collapsed-menu')) {
            $('.menu-item-label,.menu-item-arrow').removeClass('op-lg-0-force d-lg-none');
            $('body').removeClass('collapsed-menu');
            $('.show-sub + .br-menu-sub').slideDown();
        }
    }
});

$('#datatable1').DataTable({
    //"scrollY": 200,
    "scrollX": true,
    responsive: false,
    language: {
        searchPlaceholder: 'Recherche...',
        sSearch: '',
        lengthMenu: '_MENU_ éléments/page',
    }
});

// Select2
//  $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
// alert();

// alert()
//Gestion des compagnie
$('#aff_compagnie').hide();
//Ajouter un compagnie
$('#form_compagnie').on('submit', function(e) {
    e.preventDefault();
    //alert();
    $.ajax({
        type: "POST",
        cache: false,
        contentType: false,
        processData: false,
        url: "ajout_compagnie.php",
        data: new FormData(this),
        success: function(msg) {
            //  alert(msg);
            if (msg == 1) {
                var title = "Erreur",
                    message = "Cette compagnie existe déjà",
                    type = "error";
                addtoast(title, message, type);
            } else {
                // alert(msg);
                $("div.msg_erreur").hide();
                $('#ModalajoutComp').modal('toggle');
                // $("#ajout-Comp-succes").modal('show');
                //setTimeout(8000,location.reload())
                var title = "Création compagnie",
                    message = "compagnie enregistré avec succès",
                    type = "success";
                Swal.fire({
                    position: 'top-end',
                    icon: type,
                    title: message,
                    showConfirmButton: false,
                    timer: 1500
                })

                setTimeout(function() {
                     location.reload();
                }, 2500)

                // addtoast(title, message, type);

            }
        }
    });
    // }
    return false;

});



$("#type_compagnie").change(function() {
    var val = $(this).val();
    if (val == 1) {
        //alert(val);
        $('#aff_compaganie').show();
        $('#compagnie').attr('required', true);
    } else {
        $('#aff_compaganie').hide();
        $('#compagnie').attr('required', false);
    }
});



$(".statusComp").on('click', function() {
    // alert()
    var id = $(this).attr('data-id');
    //alert(id)
    $.ajax({
        type: 'GET',
        url: 'getstatus_compagnie.php',
        data: 'ref_Comp=' + id,
        success: function(msg) {
            //alert(msg);
            $(".aff_status").html(msg);
        }
    });

})


$('#btn_status').on('click', function() {
    // alert();
    $.ajax({
        type: "POST",
        url: "status_compagnie.php",
        success: function(msg) {
            // alert(msg);
            if (msg == 1) {
                $("div.msg_erreur").show();
                $("div.msg_erreur").html("impossible de supprimer").show();
            } else {
                $("#myModal_compagnie_etat").toggle();
                var title = "Vérrouillage compagnie",
                    message = "Opération éffèctuée avec succès",
                    type = "success";
                Swal.fire({
                    position: 'top-end',
                    icon: type,
                    title: message,
                    showConfirmButton: false,
                    timer: 1500
                })

                setTimeout(function() {
                    location.reload();
                }, 2000)

            }
        }
    });

    return false;

});


//supprimer compagnie
$(".supComp").on('click', function() {
    // alert()
    var id = $(this).attr('data-id');
    //alert(id)
    $.ajax({
        type: 'GET',
        url: 'getsup_compagnie.php',
        data: 'ref_comp=' + id,
        success: function(msg) {
            //alert(msg);
            $(".aff_sup").html(msg);
        }
    });

})


$('#btn_sup').on('click', function() {
    // alert();
    $.ajax({
        type: "POST",
        url: "sup_compagnie.php",
        success: function(msg) {
            // alert(msg);
            if (msg == 1) {
                $("div.msg_erreur").show();
                $("div.msg_erreur").html("impossible de supprimer").show();
            } else {
                $("#myModal_compagnie_sup").toggle();
                var title = "Vérrouillage compagnie",
                    message = "Suppression éffèctuée avec succès",
                    type = "warning";
                Swal.fire({
                    position: 'top-end',
                    icon: type,
                    title: message,
                    showConfirmButton: false,
                    timer: 1500
                })

                setTimeout(function() {
                   location.reload();
                }, 2000)

            }
        }
    });

    return false;

});

//fin supprimer 

//modifier compagnie
$(".modComp").on('click', function() {
    //alert()
    var id = $(this).attr('data-id');
    //alert(id)
    $.ajax({
        type: 'GET',
        url: 'getmodif_compagnie.php',
        data: 'ref_comp=' + id,
        success: function(msg) {
            //alert(msg);
            $(".aff_mod").html(msg);
        }
    });

})




function addtoast(title, message, type) {
    $.Toast(title, message, type, {
        has_icon: true,
        has_close_btn: true,
        stack: true,
        fullscreen: true,
        timeout: 8000,
        sticky: false,
        has_progress: true,
        rtl: false,
    });
    //location.reload();
}
//Fin ajout des utiilisateurs==============================

//Fin Gestion des compagnie
</script>
</body>

<!-- Mirrored from themepixels.me/demo/bracket/app/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 01 Sep 2020 17:52:50 GMT -->

<!-- Mirrored from localhost/tran/public/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 21 Mar 2021 23:43:54 GMT -->

</html>