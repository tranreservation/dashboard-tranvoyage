<?php

session_start();
include('../../../server.php');

$id = $_GET["ref_dest"];


$con = Server::connexion();

$con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

if($id!='')
{
$red=$con->prepare("SELECT * FROM destination WHERE id_destination=:A"); 
$red->execute(array('A'=>$id));
$util=$red->fetch();


$stit_st='Supprimer';

$_SESSION['id_destination_mod']=$util["id_destination"];

$tarif_prix_vrai = intVal($util['tarif_comp']);
$tarif_prix = $tarif_prix_vrai;
//intVal($util['tarif_prix']); 
//}
?>

<form action="#"  enctype="multipart/form-data" id="form_modif_destination">
                <div class="modal-body pd-20">

                         <div class="row">
                              <div class="col-md-4">
                                <div class="form-group">
                                <label for="">Compagnie<span style="color: red;">*</span></label>
                                <select name="compagnie_destination_id_mod" id="compagnie_destination_id_mod" class="form-control" required>
                                     <option value="">---choisir---</option>
                                     <?php 
                                     //$query="SELECT * FROM compagnie WHERE id_compagnie!='0'";
                                     if ($_SESSION['role'] == '3') {
                                      $query="SELECT * FROM compagnie WHERE id_compagnie!='0'"; 
                                     } else {
                                      $query="SELECT * FROM compagnie WHERE id_compagnie='".$_SESSION['id_compagnie']."' "; 
                                     }
                                     
                                     $comp = Server::Allquery($query,$con); 
                                foreach ($comp as $comps) {  ?>
                                     <option value="<?php echo $comps['id_compagnie']; ?>" <?php if ($util['compagnie_destination_id']==$comps['id_compagnie']) { echo 'selected'; } ?>> <?php echo $comps['nom_compagnie'] ?></option>
                                <?php } ?>
                                </select>
                                </div>
                              </div>
                              
                              <div class="col-md-4">
                              <div class="form-group">
                                <label for="">Gare de départ <span style="color: red;">*</span> </label>
                                <select name="depart_gare_id_mod" id="depart_gare_id_mod" class="form-control">
                                     <option value="">---choisir---</option>
                                     <?php $query1="SELECT * FROM gare WHERE id_gare!='0'"; $gare = Server::Allquery($query1,$con); 
                                foreach ($gare as $gares) {  ?>
                                     <option value="<?php echo $gares['id_gare']; ?>" <?php if ($util['depart_gare_id']==$gares['id_gare']) { echo 'selected'; } ?>> <?php echo $gares['nom_gare'] ?></option>
                                <?php } ?>
                                </select>
                                </div>
                              </div>

                              <div class="col-md-4">
                              <div class="form-group">
                                <label for="">Gare d'arrivée <span style="color: red;">*</span> </label>
                                <select name="arrivee_gare_id_mod" id="arrivee_gare_id_mod" class="form-control">
                                     <option value="">---choisir---</option>
                                     <?php $query2="SELECT * FROM gare WHERE id_gare!='0'"; $gare_ = Server::Allquery($query2,$con); 
                                foreach ($gare_ as $gares_) {  ?>
                                     <option value="<?php echo $gares_['id_gare']; ?>"  <?php if ($util['arrive_gare_id']==$gares_['id_gare']) { echo 'selected'; } ?>> <?php echo $gares_['nom_gare'] ?></option>
                                <?php } ?>
                                </select>
                                </div>
                              </div>

                              <div class="col-md-4">
                              <div class="form-group">
                                <label for="">Date de départ <span style="color: red;">*</span></label>
                                <input type="date" class="form-control"  value="<?php echo date("Y-m-d", strtotime($util['date_depart'])); ?>"  name="date_depart_mod" id="date_depart_mod" required>
                                </div>
                              </div>

                              <div class="col-md-2">
                              <div class="form-group">
                                <label for="">Heure départ <span style="color: red;">*</span></label>
                                <input type="time" class="form-control"  value="<?php echo $util['heure_depart'] ?>"  name="heure_depart_mod" id="heure_depart_mod" required>
                                </div>
                              </div>

                              <div class="col-md-2">
                              <div class="form-group">
                                <label for="">Heure d'arrivée <span style="color: red;"></span></label>
                                <input type="time" class="form-control"  value="<?php echo $util['heure_arrivee'] ?>"  name="heure_arrivee_mod" id="heure_arrivee_mod">
                                </div>
                              </div>

                              <div class="col-md-4">
                              <div class="form-group">
                                <label for="">Tarif <span style="color: red;">*</span></label>
                                <input type="number" class="form-control" value="<?php echo $tarif_prix ?>" name="tarif_prix_mod" id="tarif_prix_mod" required>
                                </div>
                              </div>

                         
                         </div> 
                         
                </div><!-- modal-body -->
                <div class="modal-footer">
                  <button type="submit" id="btn_modif_destination" class="btn btn-primary tx-size-xs"><i class="fas fa-save"></i> Modifier</button>
                  <button type="button" class="btn btn-danger tx-size-xs" data-dismiss="modal"><i class="fas fa-times"></i> Fermer</button>
                </div>
              </form> 


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

<script>
    $(document).ready(function(){
        var date_input=$('input[name="date_"]'); //our date input has the name "date"
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        date_input.datepicker({
            format: 'mm/dd/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
        })
    })
</script>
    <script>
        $('#form_modif_destination').on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            cache: false,
            contentType: false,
            processData: false,
            url: "mod_destination.php",
            data: new FormData(this),
            success: function(msg) {
             //alert(msg);
                if (msg == 1) {
                //  alert(msg);
                  var title="Erreur", message="Ce voyage existe déjà", type="error";
                    addtoast(title, message, type);
                }else if (msg == 2) {
                    var title="Erreur un champs est vide", message="Veuillez remplir le champs vide", type="error";
                   addtoast(title, message, type);
                } else {
                   // alert(msg);
                    $("div.msg_erreur").hide();
                    $('#myModal_destination_mod').modal('toggle');
                    var title="Modification de voyage", message="Voyage modifié avec succès", type="success";
                    Swal.fire({
                    position: 'top-end',
                    icon: type,
                    title: message,
                    showConfirmButton: false,
                    timer: 1500
                  })
                  
                  setTimeout(function() {
                      location.reload();
                    }, 2500)
                    
                   // addtoast(title, message, type);

                }
            }
        });

        return false;

    });


    function addtoast(title, message, type){
$.Toast(title, message, type, {
                    has_icon:true,
                    has_close_btn:true,
					stack: true,
                    fullscreen:true,
                    timeout:8000,
                    sticky:false,
                    has_progress:true,
                    rtl:false,
                });

            }
    //fin modifier
</script>
    
<?php } ?>              

 