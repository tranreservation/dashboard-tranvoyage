<?php

session_start();

include('../../../server.php');

$con = Server::connexion();

//echo $_SESSION['id_t_convois_sup'];
if (isset($_SESSION['id_t_convois_sup'])) {
    
$con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 

//echo $_SESSION['id_t_convois_sup'];
$red=$con->prepare("SELECT * FROM t_convois WHERE id=:A"); 
$red->execute(array('A'=>$_SESSION['id_t_convois_sup']));
$util=$red->fetch();

$libelle = $util['libelle'];

$date_creat=gmdate('Y-m-d H:i:s');

$req2=$con->prepare("DELETE FROM t_convois WHERE id=:J");

$req2->execute(array('J'=>$_SESSION['id_t_convois_sup']));

$v = "Suppression";
 //$_SESSION['nom_t_convois_rh']
 $lib_trace= $v." du convois  <b>".$libelle." </b> par ".$_SESSION['nom_prenoms'];
 Server::recupAdressIp($date_creat,$lib_trace);
 
unset($_SESSION['id_t_convois_sup']);

unset($con);

} 

?>