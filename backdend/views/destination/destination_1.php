<?php 
    session_start();
    include('../../../server.php');
    if (!isset($_SESSION['pass_rh']) && $_SESSION['pass_rh']=='' && !isset($_SESSION['id_session_dest']) && $_SESSION['id_session_dest']=='') {
     header("Location:../connexion/login.php");
}

    ?>
    <!DOCTYPE html>
<html lang="fr">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<!-- /Added by HTTrack -->

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Bracket">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="bracket/img/bracket-social.html">

    <!-- Facebook -->
    <meta property="og:url" content="">
    <meta property="og:title" content="Bracket">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="bracket/img/bracket-social.html">
    <meta property="og:image:secure_url" content="bracket/img/bracket-social.html">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>Tranvoyage</title>

    <!-- vendor css 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
    <link href="../../fontawesome-free-web/css/all.css" rel="stylesheet">
    <script defer src="../../fontawesome-free-web/js/all.js"></script>
    <link href="../../fontawesome-free-web/css/fontawesome.css" rel="stylesheet">
    <link href="../../fontawesome-free-web/css/brands.css" rel="stylesheet">
    <link href="../../fontawesome-free-web/css/solid.css" rel="stylesheet">
    <script defer src="../../fontawesome-free-web/js/brands.js"></script>
    <script defer src="../../fontawesome-free-web/js/solid.js"></script>
    <script defer src="../../fontawesome-free-web/js/fontawesome.js"></script>

    <link href="../../lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="../../lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
    <link href="../../lib/jquery-switchbutton/jquery.switchButton.css" rel="stylesheet">
    <link href="../../lib/rickshaw/rickshaw.min.css" rel="stylesheet">
    <link href="../../lib/chartist/chartist.css" rel="stylesheet">
    <link href="../../lib/datatables/jquery.dataTables.css" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="../../css/bracket.css">
     <!-- toast CSS -->
    <link  href="../../lib/toastr/toast.style.css" rel="stylesheet">
    <link rel="stylesheet" href="../../lib/sweetalert2/sweetalert2.min.css">
    <style>
    .error{
        outline: 1px solid red;
    }    
   </style>
</head>
<body>

    <!-- Bracket header.php -->
    <!-- br-header sidebar.php-->
    <?php include "../../template/header.php"; ?>
    <?php include "../../template/sidebar.php"; ?>

    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader pd-y-15 pd-l-20">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="#">Tranvoyage</a>
          <a class="breadcrumb-item" href="#">Voyages</a>
          <span class="breadcrumb-item active">Gestion des Destinations</span>
        </nav>
      </div><!-- br-pageheader -->
      <!-- <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <h4 class="tx-gray-800 mg-b-5">Data Table</h4>
        <p class="mg-b-0">DataTables is a plug-in for the jQuery Javascript library.</p>
      </div> -->

      <div class="br-pagebody">
        <div class="br-section-wrapper">

          <h6 class="tx-gray-800 tx-uppercase tx-bold tx-14 mg-b-10" >Gestion des voyages <i class="fas fa-bus "></i></h6><br>
          <button type="button" class="btn" style="width:200px;color:#fff;background:#2471A3;cursor:pointer;" data-toggle="modal" data-target="#Modalajoutdest">Ajouter un voyage  <i class="fas fa-plus-circle"></i></button>
          <br>
          <div  style="margin-bottom:10px;"></div>
          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-5p" style="text-align: center;">N°</th>
                  <th class="wd-5p" style="text-align: center;">Compagnie</th>
                  <th class="wd-15p" style="text-align: center;">Gare de départ</th>
                  <th class="wd-10p" style="text-align: center;">Gare d'arrivée</th>
                  <th class="wd-10p" style="text-align: center;">Date de départ</th>
                  <th class="wd-10p" style="text-align: center;">Heure de départ</th>
                  <th class="wd-10p" style="text-align: center;">Heure d'arrivée</th>
                  <th class="wd-10p" style="text-align: center;">Tarif</th>
                  <th class="wd-10p" style="text-align: center;">Créé le</th>
                  <th class="wd-5p" style="text-align: center;">Status</th>
                  <th class="wd-15p" style="text-align: center;">Actions</th>
                </tr>
              </thead>
              <tbody>
                <?php 

                if (setlocale(LC_TIME, 'fr_FR') == '') {
                  setlocale(LC_TIME, 'FRA');  //correction problème pour windows
                  $format_jour = '%#d';
                } else {
                  $format_jour = '%e';
                }

               // echo strftime("%A $format_jour %B %Y", strtotime('2008-04-18'));
                // affiche : vendredi 18 avril 2008
              //  echo strftime("%a $format_jour %b %Y", strtotime('2008-04-18'));
                // affiche : ven. 18 avr. 2008 

              //  $requete = "SELECT * FROM destination  WHERE id_destination!='0'";
                if ($_SESSION['role'] == '3') {
                  $requete="SELECT * FROM destination WHERE id_destination!='0' ORDER BY destination_created_at DESC"; 
                 } else {
                  $requete="SELECT * FROM destination WHERE id_destination!='0' AND compagnie_destination_id='".$_SESSION['id_compagnie']."' ORDER BY destination_created_at DESC"; 
                 }
                 
                $con = Server::connexion(); 
                $destination = Server::Allquery($requete, $con);
                //$con->query($requete)->fetchAll(); type_destination_id
                $destte_destination = $con->query($requete);
                $desttedest = $destte_destination->rowCount();
                $i=1;
                if ($desttedest>0) {

                foreach ($destination as $util) {

                  
                $requete2 = $con->query("SELECT * FROM gare WHERE id_gare='".$util['depart_gare_id']."'");
                $garedepart = $requete2->fetch();
                //  echo "SELECT * FROM gare WHERE id_gare='".$util['depart_gare_id']."'"; 
                $requete3 =  $con->query("SELECT * FROM gare WHERE id_gare='".$util['arrive_gare_id']."'");
                $garearrivee =  $requete3->fetch();

                $requete4 =  $con->query("SELECT * FROM compagnie WHERE id_compagnie='".$util['compagnie_destination_id']."'");
                $nom_compagnie = $requete4->fetch();
               
                  $state = $util['statut_destination'];
                  $tarif_prix = $util['tarif_prix'];
                  $date = gmdate("d/m/Y à H:i:s", strtotime($util['date_depart']));
                  if ($state == '0') {
                     $status = "Activé";
                     $icon_etat_dest = "fas fa-lock-open";
                     $colors= "";
                  }else {
                    $status = "Désactivé";
                    $icon_etat_dest = "fas fa-lock";
                    $colors= "#F5B7B1";
                  }
                 
                  $date_creat = gmdate("d/m/Y à H:i:s", strtotime($util['destination_created_at']));
                  $heure =$util['heure_depart'];; //
                  $heure_arrive = $util['heure_arrivee'];
                  //$date = gmdate("d/m/Y", strtotime($util['date_depart']));
                  $date_recup = gmdate("d/m/Y", strtotime($util['date_depart']));
                  $date_tab = explode('/', $date_recup); $d = $date_tab[0] + 1; $m = $date_tab[1]; $y = $date_tab[2];
                  $date = $d."/".$m."/".$y;
                 
                  //$colors= "";

                ?>
                <tr>
                  <td style="text-align: center;background:<?php echo $colors; ?>"> <?php echo $i; ?></td>
                  <td style="text-align: center;background:<?php echo $colors; ?>"><?php echo $nom_compagnie['nom_compagnie']; ?></td>
                  <td style="text-align: center;background:<?php echo $colors; ?>"><?php echo $garedepart['nom_gare']; ?></td>
                  <td style="text-align: center;background:<?php echo $colors; ?>"><?php echo $garearrivee['nom_gare']; ?></td>
                  <td style="text-align: center;background:<?php echo $colors; ?>"><?php echo $date; ?></td>
                  <td style="text-align: center;background:<?php echo $colors; ?>"><?php echo $heure; ?></td>
                  <td style="text-align: center;background:<?php echo $colors; ?>"><?php echo $heure_arrive; ?></td>
                  <td style="text-align: center;background:<?php echo $colors; ?>"><?php echo $tarif_prix; ?></td>
                  <td style="text-align: center;background:<?php echo $colors; ?>"><?php echo $date_creat; ?></td>
                  <td style="text-align: center;background:<?php echo $colors; ?>"><?php echo $status; ?></td>
                  <td style="text-align: center;background:<?php echo $colors; ?>">
                    <a class="btn btn-sm btn-success moddest" data-toggle="modal" data-target="#myModal_destination_mod" href="#" data-id="<?php echo $util['id_destination']?>"><i class="fa fa-pencil-alt"></i></a>
                    <a class="btn btn-sm btn-danger supdest" data-toggle="modal" data-target="#myModal_destination_sup" data-id="<?php echo $util['id_destination']?>" href="" ><i class="fa fa-trash-alt"></i></a>
                    <?php if($_SESSION['role'] == '3'){ ?>
                    <a class="btn btn-sm btn-warning statusdest"   data-toggle="modal" data-target="#myModal_destination_etat" data-id="<?php echo $util['id_destination']?>" href=""><i class="<?php echo $icon_etat_dest; ?>"></i></a>
                    <?php } ?>
                  </td>
                </tr>
                <?php 
                $i++; }   
                
              } else {
                echo '';
              }
                ?>

              </tbody>
            </table>
          </div><!-- table-wrapper -->

           <!--les modals--->
                  <!-- AJOUT destination MODAL -->
                  <div id="Modalajoutdest" class="modal fade">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content tx-size-sm">
                <div class="modal-header pd-x-20">
                  <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="fas fa-plus-circle"></i> Ajouter un voyage</h6>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body pd-20">
                <form action="#"  enctype="multipart/form-data" id="form_destination">
                <div class="row">
                              <div class="col-md-4">
                                <div class="form-group">
                                <label for="">Compagnie<span style="color: red;">*</span></label>
                                <select name="compagnie_destination_id" id="compagnie_destination_id" class="form-control" required>
                                     <option value="">---choisir---</option>
                                     <?php 
                                     // $query="SELECT * FROM compagnie WHERE id_compagnie!='0'"; 
                                     if ($_SESSION['role'] == '3') {
                                      $query="SELECT * FROM compagnie WHERE id_compagnie!='0'"; 
                                     } else {
                                      $query="SELECT * FROM compagnie WHERE id_compagnie='".$_SESSION['id_compagnie']."' "; 
                                     }
                                     
                                     $comp = Server::Allquery($query,$con); 
                                foreach ($comp as $comps) {  ?>
                                     <option value="<?php echo $comps['id_compagnie']; ?>"  > <?php echo $comps['nom_compagnie'] ?></option>
                                <?php } ?>
                                </select>
                                </div>
                              </div>
                              
                              <div class="col-md-4">
                              <div class="form-group">
                                <label for="">Gare de départ <span style="color: red;">*</span> </label>
                                <select name="depart_gare_id" id="depart_gare_id" class="form-control">
                                     <option value="">---choisir---</option>
                                     <?php $query="SELECT * FROM gare WHERE id_gare!='0'"; $gare = Server::Allquery($query,$con); 
                                foreach ($gare as $gares) {  ?>
                                     <option value="<?php echo $gares['id_gare']; ?>" > <?php echo $gares['nom_gare'] ?></option>
                                <?php } ?>
                                </select>
                                </div>
                              </div>
                              <div class="col-md-4">
                              <div class="form-group">
                                <label for="">Gare d'arrivée <span style="color: red;">*</span> </label>
                                <select name="arrivee_gare_id" id="arrivee_gare_id" class="form-control">
                                     <option value="">---choisir---</option>
                                     <?php $query="SELECT * FROM gare WHERE id_gare!='0'"; $gare_ = Server::Allquery($query,$con); 
                                foreach ($gare_ as $gares_) {  ?>
                                     <option value="<?php echo $gares_['id_gare']; ?>" > <?php echo $gares_['nom_gare'] ?></option>
                                <?php } ?>
                                </select>
                                </div>
                              </div>

                              <div class="col-md-3">
                              <div class="form-group">
                                <label for="">Date de départ <span style="color: red;">*</span></label>
                                <input type="date" class="form-control" name="date_depart" id="date_depart" required>
                                </div>
                              </div>
                              
                              <div class="col-md-3">
                              <div class="form-group">
                                <label for="">Heure de départ<span style="color: red;">*</span></label>
                                <input type="time" class="form-control" name="heure_depart" id="heure_depart" required>
                                </div>
                              </div>
   
                              <div class="col-md-3">
                              <div class="form-group">
                                <label for="">Heure d'arrivée <span style="color: red;">*</span></label>
                                <input type="time" class="form-control" name="heure_arrivee" id="heure_arrivee">
                                </div>
                              </div>

                              <div class="col-md-3">
                              <div class="form-group">
                                <label for="">Tarif <span style="color: red;">*</span></label>
                                <input type="number" class="form-control" name="tarif_prix" id="tarif_prix" required>
                                </div>
                              </div>

                         
                         </div>     
                         
                </div><!-- modal-body -->
                <div class="modal-footer">
                  <button type="submit" id="btn_ajout_destination" class="btn btn-success tx-size-xs"><i class="fas fa-save"></i> Enregistrer</button>
                  <button type="button" class="btn btn-danger tx-size-xs" data-dismiss="modal"><i class="fas fa-times"></i> Fermer</button>
                </div>
              </form>   
              </div>
            </div><!-- modal-dialog -->
          </div><!-- modal -->

                    <!-- MODAL ALERT MESSAGE -->
                    <div id="ajout-dest-succes" class="modal fade">
            <div class="modal-dialog" role="document">
              <div class="modal-content tx-size-sm">
                <div class="modal-body tx-center pd-y-20 pd-x-20">
                  <button type="button" class="close" data-dismiss="modal"  aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <i class="icon ion-ios-checkmark-outline tx-100 tx-success lh-1 mg-t-20 d-inline-block"></i>
                  <i class="fas fa-check"></i>
                  <h4 class="tx-success tx-semibold mg-b-20">Félicitation!</h4>
                  <p class="mg-b-20 mg-x-20">destination ajouter avec succès.</p>
                  <button type="button" class="close" data-dismiss="modal" class="btn btn-success tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium mg-b-20">
                    Continuer</button>
                  </div><!-- modal-body -->
                </div><!-- modal-content -->
              </div><!-- modal-dialog -->
            </div><!-- modal -->
          <!--FIN AJOUT destination MODAL -->


<!-- -----Modal Status destination------ -->
          <!-- SMALL MODAL -->
          <div id="myModal_destination_etat" class="modal fade">
            <div class="modal-dialog modal-sm" role="document">
              <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-x-20">
                  <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Verrouillage</h6>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body pd-20 aff_status">
                  
                </div>
                <div class="modal-footer justify-content-center">
                  <button type="button" id="btn_status" class="btn btn-primary tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium">Oui</button>
                  <button type="button" class="btn btn-danger tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal">Non</button>
                </div>
              </div>
            </div><!-- modal-dialog -->
          </div><!-- modal -->

          <!-- -----Modal supprimer destination------ -->
          <!-- SMALL MODAL -->
          <div id="myModal_destination_sup" class="modal fade">
            <div class="modal-dialog modal-sm" role="document">
              <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-x-20">
                  <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Supprimer</h6>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body pd-20 aff_sup">
                 
                </div>
                <div class="modal-footer justify-content-center">
                  <button type="button" id="btn_sup" class="btn btn-primary tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium">Oui</button>
                  <button type="button" class="btn btn-danger tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal">Non</button>
                </div>
              </div>
            </div><!-- modal-dialog -->
          </div><!-- modal -->


             <!-- MODIF destination MODAL -->
             <div id="myModal_destination_mod" class="modal fade">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content tx-size-sm">
                <div class="modal-header pd-x-20">
                  <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="fas fa-plus-circle"></i> Modifier un destination</h6>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
               <div class="aff_mod"></div> 
              </div>
            </div><!-- modal-dialog -->
          </div><!-- modal -->

                     <!--fin des modals--->

        </div><!-- br-section-wrapper -->
      </div><!-- br-pagebody -->
      <?php include "../../template/footer.php"; ?>
   
    </div><!-- br-mainpanel toast.script.js-->

    
    <script src="../../lib/jquery/jquery.js"></script>
    <script src="../../lib/popper.js/popper.js"></script>
    <script src="../../lib/bootstrap/bootstrap.js"></script>
    <script src="../../lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
    <script src="../../lib/moment/moment.js"></script>
    <script src="../../lib/jquery-ui/jquery-ui.js"></script>
    <script src="../../lib/jquery-switchbutton/jquery.switchButton.js"></script>
    <script src="../../lib/peity/jquery.peity.js"></script>
    <script src="../../lib/chartist/chartist.js"></script>
    <script src="../../lib/jquery.sparkline.bower/jquery.sparkline.min.js"></script>
    <script src="../../lib/d3/d3.js"></script>
    <script src="../../lib/rickshaw/rickshaw.min.js"></script>


    <script src="../../js/bracket.js"></script>
    <script src="../../js/ResizeSensor.js"></script>
    <script src="../../js/dashboard.js"></script>
    <script src="../../lib/datatables/jquery.dataTables.js"></script>
    <script src="../../lib/datatables-responsive/dataTables.responsive.js"></script>
    <script src="../../lib/toastr/toast.script.js"></script>
    <script src="../../lib/sweetalert2/sweetalert2.min.js"></script>
    <!-- Include Date Range Picker -->


    <!-- <script src="../../lib/js/function_destination.js"></script> -->
    <script>
    $(function() {
    'deststrict'
     
    // FOR DEMO ONLY 
    // menu collapsed by default during first page load or refresh with screen
    // having a size between 992px and 1299px. This is intended on this page only
    // for better viewing of widgets demo.
    $(window).resize(function() {
        minimizeMenu();
    });

    minimizeMenu();

    function minimizeMenu() {
        if (window.matchMedia('(min-width: 992px)').matches && window.matchMedia('(max-width: 1299px)').matches) {
            // show only the icons and hide left menu label by default
            $('.menu-item-label,.menu-item-arrow').addClass('op-lg-0-force d-lg-none');
            $('body').addClass('collapsed-menu');
            $('.show-sub + .br-menu-sub').slideUp();
        } else if (window.matchMedia('(min-width: 1300px)').matches && !$('body').hasClass('collapsed-menu')) {
            $('.menu-item-label,.menu-item-arrow').removeClass('op-lg-0-force d-lg-none');
            $('body').removeClass('collapsed-menu');
            $('.show-sub + .br-menu-sub').slideDown();
        }
    }
});

$('#datatable1').DataTable({
    //"scrollY": 200,
    "scrollX": true,
    responsive: false,
    language: {
        searchPlaceholder: 'Recherche...',
        sSearch: '',
        lengthMenu: '_MENU_ éléments/page',
    }
});

// Select2
//  $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
// alert();

// alert()
//Gestion des destination
$('#aff_destination').hide();
//Ajouter un destination
$('#form_destination').on('submit', function(e) {
    e.preventDefault();
    //alert();
    $.ajax({
        type: "POST",
        cache: false,
        contentType: false,
        processData: false,
        url: "ajout_destination.php",
        data: new FormData(this),
        success: function(msg) {
            //  alert(msg);
            if (msg == 1) {
                var title = "Erreur",
                    message = "Ce voyage existe déjà",
                    type = "error";
                addtoast(title, message, type);
            } else {
                // alert(msg);
                $("div.msg_erreur").hide();
                $('#Modalajoutdest').modal('toggle');
                // $("#ajout-dest-succes").modal('show');
                //setTimeout(8000,location.reload())
                var title = "Création destination",
                    message = "Voyage enregistré avec succès",
                    type = "success";
                Swal.fire({
                    position: 'top-end',
                    icon: type,
                    title: message,
                    showConfirmButton: false,
                    timer: 1500
                })

                setTimeout(function() {
                    location.reload();
                }, 2500)

                // addtoast(title, message, type);

            }
        }
    });
    // }
    return false;

});



$("#type_destination").change(function() {
    var val = $(this).val();
    if (val == 1) {
        //alert(val);
        $('#aff_destaganie').show();
        $('#destination').attr('required', true);
    } else {
        $('#aff_destination').hide();
        $('#destination').attr('required', false);
    }
});



$(".statusdest").on('click', function() {
    // alert()
    var id = $(this).attr('data-id');
   // alert(id)
    $.ajax({
        type: 'GET',
        url: 'getstatus_destination.php',
        data: 'ref_dest=' + id,
        success: function(msg) {
            //alert(msg);
            $(".aff_status").html(msg);
        }
    });

})


$('#btn_status').on('click', function() {
    // alert();
    $.ajax({
        type: "POST",
        url: "status_destination.php",
        success: function(msg) {
            // alert(msg);
            if (msg == 1) {
                $("div.msg_erreur").show();
                $("div.msg_erreur").html("impossible de supprimer").show();
            } else {
                $("#myModal_destination_etat").toggle();
                var title = "Vérrouillage voyage",
                    message = "Opération éffèctuée avec succès",
                    type = "success";
                Swal.fire({
                    position: 'top-end',
                    icon: type,
                    title: message,
                    showConfirmButton: false,
                    timer: 1500
                })

                setTimeout(function() {
                    location.reload();
                }, 2000)

            }
        }
    });

    return false;

});


//supprimer destination
$(".supdest").on('click', function() {
    // alert()
    var id = $(this).attr('data-id');
    //alert(id)
    $.ajax({
        type: 'GET',
        url: 'getsup_destination.php',
        data: 'ref_dest=' + id,
        success: function(msg) {
            //alert(msg);
            $(".aff_sup").html(msg);
        }
    });

})


$('#btn_sup').on('click', function() {
    // alert();
    $.ajax({
        type: "POST",
        url: "sup_destination.php",
        success: function(msg) {
            // alert(msg);
            if (msg == 1) {
                $("div.msg_erreur").show();
                $("div.msg_erreur").html("impossible de supprimer").show();
            } else {
                $("#myModal_destination_sup").toggle();
                var title = "Vérrouillage voyage",
                    message = "Suppression éffèctuée avec succès",
                    type = "warning";
                Swal.fire({
                    position: 'top-end',
                    icon: type,
                    title: message,
                    showConfirmButton: false,
                    timer: 1500
                })

                setTimeout(function() {
                   location.reload();
                }, 2000)

            }
        }
    });

    return false;

});

//fin supprimer 

//modifier destination
$(".moddest").on('click', function() {
    //alert()
    var id = $(this).attr('data-id');
    //alert(id)
    $.ajax({
        type: 'GET',
        url: 'getmodif_destination.php',
        data: 'ref_dest=' + id,
        success: function(msg) {
            //alert(msg);
            $(".aff_mod").html(msg);
        }
    });

})




function addtoast(title, message, type) {
    $.Toast(title, message, type, {
        has_icon: true,
        has_close_btn: true,
        stack: true,
        fullscreen: true,
        timeout: 8000,
        sticky: false,
        has_progress: true,
        rtl: false,
    });
    //location.reload();
}
//Fin ajout des utiilisateurs==============================

//Fin Gestion des destination
</script>
</body>

<!-- Mirrored from themepixels.me/demo/bracket/app/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 01 Sep 2020 17:52:50 GMT -->

<!-- Mirrored from localhost/tran/public/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 21 Mar 2021 23:43:54 GMT -->

</html>