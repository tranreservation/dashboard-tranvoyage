<?php 
    session_start();
    include('../../../server.php');
    if (!isset($_SESSION['pass_rh']) && $_SESSION['pass_rh']=='' && !isset($_SESSION['id_session_dest']) && $_SESSION['id_session_dest']=='') {
     header("Location:../connexion/login.php");
}

    ?>
    <!DOCTYPE html>
<html lang="fr">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<!-- /Added by HTTrack -->

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Bracket">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="bracket/img/bracket-social.html">

    <!-- Facebook -->
    <meta property="og:url" content="">
    <meta property="og:title" content="Bracket">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="bracket/img/bracket-social.html">
    <meta property="og:image:secure_url" content="bracket/img/bracket-social.html">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>Tranvoyage</title>

    <!-- vendor css 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
    <link href="../../fontawesome-free-web/css/all.css" rel="stylesheet">
    <script defer src="../../fontawesome-free-web/js/all.js"></script>
    <link href="../../fontawesome-free-web/css/fontawesome.css" rel="stylesheet">
    <link href="../../fontawesome-free-web/css/brands.css" rel="stylesheet">
    <link href="../../fontawesome-free-web/css/solid.css" rel="stylesheet">
    <script defer src="../../fontawesome-free-web/js/brands.js"></script>
    <script defer src="../../fontawesome-free-web/js/solid.js"></script>
    <script defer src="../../fontawesome-free-web/js/fontawesome.js"></script>

    <link href="../../lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="../../lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
    <link href="../../lib/jquery-switchbutton/jquery.switchButton.css" rel="stylesheet">
    <link href="../../lib/rickshaw/rickshaw.min.css" rel="stylesheet">
    <link href="../../lib/chartist/chartist.css" rel="stylesheet">
    <link href="../../lib/datatables/jquery.dataTables.css" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="../../css/bracket.css">
     <!-- toast CSS -->
    <link  href="../../lib/toastr/toast.style.css" rel="stylesheet">
    <link rel="stylesheet" href="../../lib/sweetalert2/sweetalert2.min.css">
    <style>
    .error{
        outline: 1px solid red;
    }    
   </style>
</head>
<body>

    <!-- Bracket header.php -->
    <!-- br-header sidebar.php-->
    <?php include "../../template/header.php"; ?>
    <?php include "../../template/sidebar.php"; ?>

    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader pd-y-15 pd-l-20">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="#">Tranvoyage</a>
          <a class="breadcrumb-item" href="#">Tickets</a>
          <span class="breadcrumb-item active">Gestion des tickets</span>
        </nav>
      </div><!-- br-pageheader -->
      <!-- <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <h4 class="tx-gray-800 mg-b-5">Data Table</h4>
        <p class="mg-b-0">DataTables is a plug-in for the jQuery Javascript library.</p>
      </div> -->

      <div class="br-pagebody">
        <div class="br-section-wrapper">

          <h6 class="tx-gray-800 tx-uppercase tx-bold tx-14 mg-b-10" >Liste des tickets en cours de paiement <i class="fas fa-ticket-alt"></i></h6><br>
          <div class="row">
              &nbsp;&nbsp;&nbsp;&nbsp;<p>&nbsp;Paiement(s) passé(s)&nbsp;</p><div class="col-md-2" style="background-color: #E74C3C;height:20px;">
            </div>
            <p> &nbsp;&nbsp;&nbsp;&nbsp;Paiement(s) en cours&nbsp;&nbsp;</p><div class="col-md-2" style="background-color: #52BE80;height:20px;">
            </div>
            <p> &nbsp;&nbsp;&nbsp;&nbsp;Paiement(s) éffectué&nbsp;&nbsp;</p><div class="col-md-2" style="background-color: #196F3D;height:20px;">
            </div>
             <div  style="margin-bottom:10px;"></div>
          </div> <br>
          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-5p" style="text-align: center;">N°</th>
                  <th class="wd-5p" style="text-align: center;">Code ticket client</th>
                  <th class="wd-15p" style="text-align: center;">Gare de départ</th>
                  <th class="wd-10p" style="text-align: center;">Compagnie</th>
                  <th class="wd-10p" style="text-align: center;">Nombre de ticket</th>
                  <th class="wd-10p" style="text-align: center;">Montant unitaire</th>
                  <th class="wd-10p" style="text-align: center;">Montant total</th>
                  <th class="wd-10p" style="text-align: center;">Date de paiement</th>        
                  <th class="wd-5p" style="text-align: center;">Status du paiement</th>
                  <th class="wd-5p" style="text-align: center;">Actions</th>
                </tr>
              </thead>
              <tbody>
                <?php 

                if (setlocale(LC_TIME, 'fr_FR') == '') {
                  setlocale(LC_TIME, 'FRA');  //correction problème pour windows
                  $format_jour = '%#d';
                } else {
                  $format_jour = '%e';
                }

               // echo strftime("%A $format_jour %B %Y", strtotime('2008-04-18'));
                // affiche : vendredi 18 avril 2008
              //  echo strftime("%a $format_jour %b %Y", strtotime('2008-04-18'));
                // affiche : ven. 18 avr. 2008 

              //  $requete = "SELECT * FROM paiement  WHERE id_paiement!='0'";
                if ($_SESSION['role'] == '3') {
                  $requete="SELECT * FROM paiement WHERE id_paiement!='0'"; 
                 } else {
                  $requete="SELECT * FROM paiement WHERE id_paiement!='0' AND compagnie_paiement_id='".$_SESSION['id_compagnie']."' "; 
                 }
                 
                $con = Server::connexion(); 
                $paiement = Server::Allquery($requete, $con);
                //$con->query($requete)->fetchAll(); type_paiement_id
                $destte_paiement = $con->query($requete);
                $desttedest = $destte_paiement->rowCount();
                $i=1;
                if ($desttedest>0) {

                foreach ($paiement as $util) {

                  
                $requete2 = $con->query("SELECT * FROM gare WHERE id_gare='".$util['depart_id']."'");
                $garedepart = $requete2->fetch();
                //  echo "SELECT * FROM gare WHERE id_gare='".$util['depart_gare_id']."'"; 
                $requete3 =  $con->query("SELECT * FROM gare WHERE id_gare='".$util['arrive_id']."'");
                $garearrivee =  $requete3->fetch();

                $requete4 =  $con->query("SELECT * FROM compagnie WHERE id_compagnie='".$util['compagnie_paiement_id']."'");
                $nom_compagnie = $requete4->fetch();
               

                  if ($util['status_paiement'] == '0') {
                     $status = "Paiement en cours";
                     $colors= "#52BE80;color:#fff;";
                     $icon_etat_dest = "fas fa-lock";
                  }else if($util['status_paiement'] == '1') {
                    $status = "Paiement éffectué";
                    $colors= "#196F3D;color:#fff;";
                    $icon_etat_dest = "fas fa-check";
                  } else {
                    $status = "Ticket acheté";
                    $colors= ""; 
                    $icon_etat_dest = "fas fa-check";
                  }

                  if ($util['date_paiement'] < gmdate("Y-m-d")) {
                     $colors= "#E74C3C;color:#fff;";
                     $status = "Paiement déjà passé";
                  }
                 

                  $date = date("d/m/Y H:i:s", strtotime($util['date_paiement']));
                 
                  //$colors= "";

                ?>
                <tr>
                  <td style="text-align: center;background:<?php echo $colors; ?>"> <?php echo $i; ?></td>
                  <td style="text-align: center;background:<?php echo $colors; ?>"><?php echo $util['code_ticket']; ?></td>
                  <td style="text-align: center;background:<?php echo $colors; ?>"><?php echo $garedepart['nom_gare']; ?></td>
                  <td style="text-align: center;background:<?php echo $colors; ?>"><?php echo $nom_compagnie['nom_compagnie']; ?></td>
                  <td style="text-align: center;background:<?php echo $colors; ?>"><?php echo $util['nombre_ticket']; ?></td>
                  <td style="text-align: center;background:<?php echo $colors; ?>"><?php echo $util['mont_ticket_unitaire']; ?></td>
                  <td style="text-align: center;background:<?php echo $colors; ?>"><?php echo $util['montant_total_ticket']; ?></td>
                  <td style="text-align: center;background:<?php echo $colors; ?>"><?php echo $date; ?></td>
                  <td style="text-align: center;background:<?php echo $colors; ?>"><?php echo $status; ?></td>
                  <td style="text-align: center;background:<?php echo $colors; ?>">
                    <?php if($_SESSION['role'] == '3' || $_SESSION['role'] == '2'){ ?>
                    <a class="btn btn-sm btn-warning statusval"   data-toggle="modal" data-target="#myModal_paiement_etat" data-id="<?php echo $util['id_paiement']?>" href=""><i class="<?php echo $icon_etat_dest; ?>"></i></a>
                    <?php } ?>
                  </td>
    
                </tr>
                <?php 
                $i++; }   
                
              } else {
                echo '';
              }
                ?>

              </tbody>
            </table>


            <!-- -----Modal Status paiement------ -->
          <!-- SMALL MODAL -->
          <div id="myModal_paiement_etat" class="modal fade">
            <div class="modal-dialog modal-sm" role="document">
              <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-x-20">
                  <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Validation</h6>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body pd-20 aff_validation">
                  
                </div>
                <div class="modal-footer justify-content-center">
                  <button type="button" id="btn_status" class="btn btn-primary tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium">Oui</button>
                  <button type="button" class="btn btn-danger tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal">Non</button>
                </div>
              </div>
            </div><!-- modal-dialog -->
          </div><!-- modal -->

          <!-- -----Modal supprimer paiement------ -->

          </div><!-- table-wrapper -->
        </div><!-- br-section-wrapper -->
      </div><!-- br-pagebody -->
      <?php include "../../template/footer.php"; ?>
   
    </div><!-- br-mainpanel toast.script.js-->

    
    <script src="../../lib/jquery/jquery.js"></script>
    <script src="../../lib/popper.js/popper.js"></script>
    <script src="../../lib/bootstrap/bootstrap.js"></script>
    <script src="../../lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
    <script src="../../lib/moment/moment.js"></script>
    <script src="../../lib/jquery-ui/jquery-ui.js"></script>
    <script src="../../lib/jquery-switchbutton/jquery.switchButton.js"></script>
    <script src="../../lib/peity/jquery.peity.js"></script>
    <script src="../../lib/chartist/chartist.js"></script>
    <script src="../../lib/jquery.sparkline.bower/jquery.sparkline.min.js"></script>
    <script src="../../lib/d3/d3.js"></script>
    <script src="../../lib/rickshaw/rickshaw.min.js"></script>


    <script src="../../js/bracket.js"></script>
    <script src="../../js/ResizeSensor.js"></script>
    <script src="../../js/dashboard.js"></script>
    <script src="../../lib/datatables/jquery.dataTables.js"></script>
    <script src="../../lib/datatables-responsive/dataTables.responsive.js"></script>
    <script src="../../lib/toastr/toast.script.js"></script>
    <script src="../../lib/sweetalert2/sweetalert2.min.js"></script>
    <!-- Include Date Range Picker -->


    <!-- <script src="../../lib/js/function_paiement.js"></script> -->
    <script>
    $(function() {
    'deststrict'
     
    // FOR DEMO ONLY 
    // menu collapsed by default during first page load or refresh with screen
    // having a size between 992px and 1299px. This is intended on this page only
    // for better viewing of widgets demo.
    $(window).resize(function() {
        minimizeMenu();
    });

    minimizeMenu();

    function minimizeMenu() {
        if (window.matchMedia('(min-width: 992px)').matches && window.matchMedia('(max-width: 1299px)').matches) {
            // show only the icons and hide left menu label by default
            $('.menu-item-label,.menu-item-arrow').addClass('op-lg-0-force d-lg-none');
            $('body').addClass('collapsed-menu');
            $('.show-sub + .br-menu-sub').slideUp();
        } else if (window.matchMedia('(min-width: 1300px)').matches && !$('body').hasClass('collapsed-menu')) {
            $('.menu-item-label,.menu-item-arrow').removeClass('op-lg-0-force d-lg-none');
            $('body').removeClass('collapsed-menu');
            $('.show-sub + .br-menu-sub').slideDown();
        }
    }
});

$('#datatable1').DataTable({
    //"scrollY": 200,
    "scrollX": true,
    responsive: false,
    language: {
        searchPlaceholder: 'Recherche...',
        sSearch: '',
        lengthMenu: '_MENU_ éléments/page',
    }
});

$(".statusval").on('click', function() {
// alert()
    var id = $(this).attr('data-id');
  // alert(id)
    $.ajax({
        type: 'GET',
        url: 'getstatus_paiement.php',
        data: 'ref_dest=' + id,
        success: function(msg) {
            //alert(msg);
            $(".aff_validation").html(msg);
        }
    });

})



$('#btn_status').on('click', function() {
   // alert();
    $.ajax({
        type: "POST",
        url: "valide_paiement.php",
        success: function(msg) {
          //  alert(msg);
            if (msg == 1) {
                var title = "Attention !",
                    message = "Impossible de valider le ticket car le client n'a pas encore terminer son paiement ou la date du paiement est déjà passé !",
                    type = "error";
                addtoast(title, message, type);
            } else {
                $("#myModal_paiement_etat").toggle();
                var title = "Validation paie",
                    message = "Opération éffèctuée avec succès",
                    type = "success";
                Swal.fire({
                    position: 'top-end',
                    icon: type,
                    title: message,
                    showConfirmButton: false,
                    timer: 1500
                })

                setTimeout(function() {
                   location.reload();
                }, 2000)

            }
        }
    });

    return false;

});


function addtoast(title, message, type) {
    $.Toast(title, message, type, {
        has_icon: true,
        has_close_btn: true,
        stack: true,
        fullscreen: true,
        timeout: 8000,
        sticky: false,
        has_progress: true,
        rtl: false,
    });
    //location.reload();
}


</script>
</body>

<!-- Mirrored from themepixels.me/demo/bracket/app/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 01 Sep 2020 17:52:50 GMT -->

<!-- Mirrored from localhost/tran/public/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 21 Mar 2021 23:43:54 GMT -->

</html>