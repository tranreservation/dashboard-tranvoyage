<?php 
    session_start();
    include('../../../server.php');
    if (!isset($_SESSION['pass_rh']) && $_SESSION['pass_rh']=='' && !isset($_SESSION['id_session_gare']) && $_SESSION['id_session_gare']=='') {
     header("Location:../connexion/login.php");
}

    ?>
    <!DOCTYPE html>
<html lang="fr">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<!-- /Added by HTTrack -->

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Bracket">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="bracket/img/bracket-social.html">

    <!-- Facebook -->
    <meta property="og:url" content="">
    <meta property="og:title" content="Bracket">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="bracket/img/bracket-social.html">
    <meta property="og:image:secure_url" content="bracket/img/bracket-social.html">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>Tranvoyage</title>

    <!-- vendor css 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
    <link href="../../fontawesome-free-web/css/all.css" rel="stylesheet">
    <script defer src="../../fontawesome-free-web/js/all.js"></script>
    <link href="../../fontawesome-free-web/css/fontawesome.css" rel="stylesheet">
    <link href="../../fontawesome-free-web/css/brands.css" rel="stylesheet">
    <link href="../../fontawesome-free-web/css/solid.css" rel="stylesheet">
    <script defer src="../../fontawesome-free-web/js/brands.js"></script>
    <script defer src="../../fontawesome-free-web/js/solid.js"></script>
    <script defer src="../../fontawesome-free-web/js/fontawesome.js"></script>

    <link href="../../lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="../../lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
    <link href="../../lib/jquery-switchbutton/jquery.switchButton.css" rel="stylesheet">
    <link href="../../lib/rickshaw/rickshaw.min.css" rel="stylesheet">
    <link href="../../lib/chartist/chartist.css" rel="stylesheet">
    <link href="../../lib/datatables/jquery.dataTables.css" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="../../css/bracket.css">
     <!-- toast CSS -->
    <link  href="../../lib/toastr/toast.style.css" rel="stylesheet">
    <link rel="stylesheet" href="../../lib/sweetalert2/sweetalert2.min.css">
    <style>
    .error{
        outline: 1px solid red;
    }    
   </style>
</head>
<body>

    <!-- Bracket header.php -->
    <!-- br-header sidebar.php-->
    <?php include "../../template/header.php"; ?>
    <?php include "../../template/sidebar.php"; ?>

    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader pd-y-15 pd-l-20">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="#">Tranvoyage</a>
          <a class="breadcrumb-item" href="#">Historique</a>
          <span class="breadcrumb-item active">Historique de l'application</span>
        </nav>
      </div><!-- br-pageheader -->
      <!-- <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <h4 class="tx-gray-800 mg-b-5">Data Table</h4>
        <p class="mg-b-0">DataTables is a plug-in for the jQuery Javascript library.</p>
      </div> -->

      <div class="br-pagebody">
        <div class="br-section-wrapper">

          <h6 class="tx-gray-800 tx-uppercase tx-bold tx-14 mg-b-10" >Historique de l'application  <i class="fa fa-book tx-20"></i></h6><br>
        
          <br>
          <div  style="margin-bottom:10px;"></div>
          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-5p" style="text-align: center;">N°</th>
                  <th class="wd-15p" style="text-align: center;">Date</th>
                  <th class="wd-15p" style="text-align: center;">Actions éffectuées</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                $requete = "SELECT * FROM trace WHERE id_trace!='0' ORDER BY date_trace DESC";
                $con = Server::connexion(); 
                $histo = Server::Allquery($requete, $con);
                //$con->query($requete)->fetchAll(); type_histo_id
                $historique = $con->query($requete);
                $histo_compte = $historique->rowCount();
                $i=1;
                if ($histo_compte>0) {

                foreach ($historique as $util) {
                 
                  $date = date("d/m/Y à H:i:s", strtotime($util['date_trace']));
                 
                  $colors= "";

                ?>
                <tr>
                  <td style="text-align: center;background:<?php echo $colors; ?>"> <?php echo $i; ?></td>
                  <td style="text-align: center;background:<?php echo $colors; ?>"><?php echo $date; ?></td>
                  <td style="text-align: center;background:<?php echo $colors; ?>"><?php echo $util['lib_trace']; ?></td>
                </tr>
                <?php 
                $i++; }   
                
              } else {
                echo '';
              }
                ?>

              </tbody>
            </table>
          </div><!-- table-wrapper -->

           <!--les modals--->

        </div><!-- br-section-wrapper -->
      </div><!-- br-pagebody -->
      <?php include "../../template/footer.php"; ?>
   
    </div><!-- br-mainpanel toast.script.js-->

    
    <script src="../../lib/jquery/jquery.js"></script>
    <script src="../../lib/popper.js/popper.js"></script>
    <script src="../../lib/bootstrap/bootstrap.js"></script>
    <script src="../../lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
    <script src="../../lib/moment/moment.js"></script>
    <script src="../../lib/jquery-ui/jquery-ui.js"></script>
    <script src="../../lib/jquery-switchbutton/jquery.switchButton.js"></script>
    <script src="../../lib/peity/jquery.peity.js"></script>
    <script src="../../lib/chartist/chartist.js"></script>
    <script src="../../lib/jquery.sparkline.bower/jquery.sparkline.min.js"></script>
    <script src="../../lib/d3/d3.js"></script>
    <script src="../../lib/rickshaw/rickshaw.min.js"></script>


    <script src="../../js/bracket.js"></script>
    <script src="../../js/ResizeSensor.js"></script>
    <script src="../../js/dashboard.js"></script>
    <script src="../../lib/datatables/jquery.dataTables.js"></script>
    <script src="../../lib/datatables-responsive/dataTables.responsive.js"></script>
    <script src="../../lib/toastr/toast.script.js"></script>
    <script src="../../lib/sweetalert2/sweetalert2.min.js"></script>
    <!-- <script src="../../lib/js/function_gare.js"></script> -->
    <script>
    $(function() {
    'garestrict'
     
    // FOR DEMO ONLY 
    // menu collapsed by default during first page load or refresh with screen
    // having a size between 992px and 1299px. This is intended on this page only
    // for better viewing of widgets demo.
    $(window).resize(function() {
        minimizeMenu();
    });

    minimizeMenu();

    function minimizeMenu() {
        if (window.matchMedia('(min-width: 992px)').matches && window.matchMedia('(max-width: 1299px)').matches) {
            // show only the icons and hide left menu label by default
            $('.menu-item-label,.menu-item-arrow').addClass('op-lg-0-force d-lg-none');
            $('body').addClass('collapsed-menu');
            $('.show-sub + .br-menu-sub').slideUp();
        } else if (window.matchMedia('(min-width: 1300px)').matches && !$('body').hasClass('collapsed-menu')) {
            $('.menu-item-label,.menu-item-arrow').removeClass('op-lg-0-force d-lg-none');
            $('body').removeClass('collapsed-menu');
            $('.show-sub + .br-menu-sub').slideDown();
        }
    }
});

$('#datatable1').DataTable({
    //"scrollY": 200,
    "scrollX": true,
    responsive: false,
    language: {
        searchPlaceholder: 'Recherche...',
        sSearch: '',
        lengthMenu: '_MENU_ éléments/page',
    }
});

// Select2
//  $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
// alert();

// alert()
//Gestion des gare
$('#aff_gare').hide();
//Ajouter un gare

//modifier gare

function addtoast(title, message, type) {
    $.Toast(title, message, type, {
        has_icon: true,
        has_close_btn: true,
        stack: true,
        fullscreen: true,
        timeout: 8000,
        sticky: false,
        has_progress: true,
        rtl: false,
    });
    //location.reload();
}
//Fin ajout des utiilisateurs==============================

//Fin Gestion des gare
</script>
</body>

<!-- Mirrored from themepixels.me/demo/bracket/app/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 01 Sep 2020 17:52:50 GMT -->

<!-- Mirrored from localhost/tran/public/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 21 Mar 2021 23:43:54 GMT -->

</html>