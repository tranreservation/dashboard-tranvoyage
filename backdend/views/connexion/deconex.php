
<?php

session_start();
include('../../../server.php');

$con = Server::connexion();

$req=$con->prepare("UPDATE utilisateur SET connecte=:A WHERE secur=:B");
$req->execute(array('A'=>'non', 'B'=>$_SESSION['id_session_user']));

if(isset($_SESSION['pass_rh']) && $_SESSION['pass_rh']!=''){ unset($_SESSION['pass_rh']); }
if(isset($_SESSION['id_session_user']) && $_SESSION['id_session_user']!=''){ unset($_SESSION['id_session_user']); }

header('Location:login.php'); 
    
?>
