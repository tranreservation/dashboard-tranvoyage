<!DOCTYPE html>
<html lang="fr">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Bracket">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="bracket/img/bracket-social.html">

    <!-- Facebook -->
    <meta property="og:url" content="">
    <meta property="og:title" content="Bracket">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="bracket/img/bracket-social.html">
    <meta property="og:image:secure_url" content="bracket/img/bracket-social.html">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Tranvoyage Administration.">
    <meta name="author" content="ThemePixels">

    <title>Tranvoyage</title>

    <!-- vendor css 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->


    <!-- vendor css -->
    <link href="../../lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="../../lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link  href="../../lib/toastr/toast.style.css" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="../../css/bracket.css">
    
    <style>
    .error{
        outline: 1px solid red;
    }    
   </style>
</head>


  <body>

    <div class="d-flex align-items-center justify-content-center bg-br-primary ht-100v">

      <div class="login-wrapper wd-300 wd-xs-350 pd-25 pd-xs-40 bg-white rounded shadow-base">
        <div class="signin-logo tx-center tx-28 tx-bold tx-inverse"><img src="../../../img/logo.jpg" height="80px" width="200px" alt="" > </div>
        <div class="tx-center mg-b-60">
        Entrez l'adresse &eacute;lectronique utilis&eacute;e pour la création de votre compte. Un courriel vous sera envoyé avec un lien de r&eacute;initialisation de mot de passe.
        </div>
        <form action="" method="post" id="form_mot" name="form_mot" autocomplete="off">
        <div class="form-group">
          <input type="email" class="form-control" placeholder="Veuillez saisir votre adresse email" name="email" id="email" autocomplete="off" required>
        </div><!-- form-group -->
        <div class="msg_con"> 
            <div class="msg_error"></div>
            <div class="msg_ok"></div>
        </div>
        <button type="submit" id="btn_submit" class="btn btn-info btn-block">Envoyer</button>
         </form>
        <!-- <div class="mg-t-60 tx-center">Not yet a member? <a href="#" class="tx-info">Sign Up</a></div> -->
      </div><!-- login-wrapper -->
    </div><!-- d-flex -->

    
   
    <script src="../../lib/jquery/jquery.js"></script>
    <script src="../../lib/popper.js/popper.js"></script>
    <script src="../../lib/bootstrap/bootstrap.js"></script>
    <script src="../../lib/toastr/toast.script.js"></script>
    <script src="../../lib/js/co_form.js"></script>
 

  </body>

<!-- Mirrored from themepixels.me/demo/bracket/app/signin-simple.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 14 Apr 2021 09:16:06 GMT -->
</html>
