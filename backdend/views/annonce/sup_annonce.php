<?php
session_start();
include('../../../server.php');

$con = Server::connexion();

if (isset($_SESSION['id_annonce_sup'])) {
    
$con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 

$date_creat=gmdate('Y-m-d H:i:s');

$req2=$con->prepare("DELETE FROM annonces WHERE id_annonces=:J");
$req2->execute(array('J'=>$_SESSION['id_annonce_sup']));

$v = "Suppression";
 //$_SESSION['nom_annonce_rh']
 $lib_trace= $v." d'une annonce éffectuée par ".$_SESSION['nom_prenoms'];
 Server::recupAdressIp($date_creat,$lib_trace);
 
unset($_SESSION['id_annonce_sup']);
unset($con);
} 

?>