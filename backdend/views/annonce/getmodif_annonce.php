<?php

session_start();
include('../../../server.php');

$id = $_GET["ref_annonces"];


$con = Server::connexion();

$con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

if($id!='')
{
$red=$con->prepare("SELECT * FROM annonces WHERE id_annonces=:A"); 
$red->execute(array('A'=>$id));
$util=$red->fetch();

$stit_st='Supprimer';

$_SESSION['id_annonces_mod']=$util["id_annonces"];
//}
?>

<form action="#"  enctype="multipart/form-data" id="form_modif_annonces">
                <div class="modal-body pd-20">
                          <div class="row">
                              <div class="col-md-4">
                                   <div class="form-group">
                                        <label for="">Libéllé de l'annonce <span style="color: red;"></span></label><br>
                                          <textarea name="libelle_annonce_mod" id="libelle_annonce_mod" cols="40" rows="5" minlength="20" required><?php echo $util['libelle_annonces']; ?></textarea>
                                   </div>
                              </div>                  
                         </div>     
                         
                </div><!-- modal-body -->
                <div class="modal-footer">
                  <button type="submit" id="btn_modif_annonces" class="btn btn-primary tx-size-xs"><i class="fas fa-save"></i> Modifier</button>
                  <button type="button" class="btn btn-danger tx-size-xs" data-dismiss="modal"><i class="fas fa-times"></i> Fermer</button>
                </div>
              </form> 



    <script>
        $('#form_modif_annonces').on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            cache: false,
            contentType: false,
            processData: false,
            url: "mod_annonce.php",
            data: new FormData(this),
            success: function(msg) {
             //alert(msg);
                if (msg == 1) {
                //  alert(msg);
                  var title="Erreur", message="Cette annonce existe déjà", type="error";
                    addtoast(title, message, type);
                }else if (msg == 2) {
                    var title="Erreur un champs est vide", message="Veuillez remplir le champs vide", type="error";
                   addtoast(title, message, type);
                } else {
                   // alert(msg);
                    $("div.msg_erreur").hide();
                    $('#myModal_annonces_mod').modal('toggle');
                    var title="Modification annonce", message="annonce modifiée avec succès", type="success";
                    Swal.fire({
                    position: 'top-end',
                    icon: type,
                    title: message,
                    showConfirmButton: false,
                    timer: 1500
                  })
                  
                  setTimeout(function() {
                          location.reload();
                    }, 2500)
                    
                   // addtoast(title, message, type);

                }
            }
        });

        return false;

    });



    function addtoast(title, message, type){
$.Toast(title, message, type, {
                    has_icon:true,
                    has_close_btn:true,
					stack: true,
                    fullscreen:true,
                    timeout:8000,
                    sticky:false,
                    has_progress:true,
                    rtl:false,
                });

            }
    //fin modifier
</script>
    
<?php } ?>              

 