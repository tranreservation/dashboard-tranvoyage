<?php 

if ($_SESSION['role'] == '3') {
$requete="SELECT * FROM paiement WHERE id_paiement!='0'"; 
} else {
$requete="SELECT * FROM paiement WHERE id_paiement!='0' AND compagnie_paiement_id='".$_SESSION['id_compagnie']."' "; 
}

if ($_SESSION['role'] == '3') {
$requete1="SELECT * FROM paiement WHERE id_paiement!='0' AND status_paiement='1' "; 
} else {
$requete1="SELECT * FROM paiement WHERE id_paiement!='0' AND compagnie_paiement_id='".$_SESSION['id_compagnie']."' AND status_paiement='1'"; 
}

$con = Server::connexion(); 
$destte_paiement = $con->query($requete);
$desttedest = $destte_paiement->rowCount();

$convois = $con->query("SELECT * FROM t_convois WHERE id != '0'");
$compteConvois = $convois->rowCount();

$convoisAttente = $con->query("SELECT * FROM t_convois WHERE params_status_convois_id = '1'");
$compteConvoisAttente = $convoisAttente->rowCount();

$convoisValider = $con->query("SELECT * FROM t_convois WHERE params_status_convois_id = '2'");
$compteConvoisValider = $convoisValider->rowCount();


$destte_paiement1 = $con->query($requete1);
$desttedest1 = $destte_paiement1->rowCount();

$requete = "SELECT * FROM affectations 
LEFT JOIN utilisateurs ON affectations.utilisateurs_affectation_id=utilisateurs.id_utilisateurs
LEFT JOIN gare ON affectations.gare_affectation_id=gare.id_gare
LEFT JOIN compagnie ON utilisateurs.compagnie_id=compagnie.id_compagnie
WHERE id_affectations!='0' ";

$con = Server::connexion(); 
$affectationste_affectations = $con->query($requete);
$compte_affectations = $affectationste_affectations->rowCount();
$lien = "./views/affectation/affectation.php";

if ($_SESSION['role']!=3) {
    $compte_affectations = "Espace admin";
    $lien = "#";
}


?>

<div class="row row-sm">
                <div class="col-sm-6 col-xl-3">
                   <a href="./views/destination/destination.php" style="text-decoration:none;"><div class="bg-teal rounded overflow-hidden">
                        <div class="pd-25 d-flex align-items-center">
                            <i class="fas fa-bus tx-60 lh-0 tx-white op-7"></i>
                            <div class="mg-l-20">
                                <p class="tx-17 tx-spacing-1 tx-mont  tx-bold tx-uppercase tx-white-8 mg-b-10">Convois</p>
                                <p class="tx-24 tx-white tx-lato tx-bold mg-b-2 lh-1"> <?php echo $compteConvois; ?> </p>
                                <span class="tx-11 tx-roboto tx-white-6"></span>
                            </div>
                        </div>
                    </a></div>
                </div>
                <!-- col-3 -->
                <div class="col-sm-6 col-xl-3 mg-t-20 mg-sm-t-0">
                   <a href="#" style="text-decoration:none;"><div class="bg-primary rounded overflow-hidden">
                        <div class="pd-25 d-flex align-items-center">
                            <i class="fa fa-bus tx-60 lh-0 tx-white op-7"></i>
                            <div class="mg-l-20"> 
                                <p class="tx-17 tx-spacing-1 tx-mont  tx-bold tx-uppercase tx-white-8 mg-b-10">Validé(s)</p>
                                <p class="tx-24 tx-white tx-lato tx-bold mg-b-2 lh-1"><?php echo $compteConvoisValider; ?></p>
                                <span class="tx-11 tx-roboto tx-white-6"></span>
                            </div>
                        </div>
                    </a></div>
                </div>
                <!-- col-3 -->
                <div class="col-sm-6 col-xl-3 mg-t-20 mg-xl-t-0">
                   <a href="<?php echo $lien; ?>" style="text-decoration:none;"><div class="bg-info rounded overflow-hidden">
                        <div class="pd-25 d-flex align-items-center">
                            <i class="fa fa-bus tx-60 lh-0 tx-white op-7"></i>
                            <div class="mg-l-20">
                                <p class="tx-14 tx-spacing-1 tx-mont  tx-bold tx-uppercase tx-white-8 mg-b-10">En attente</p>
                                <p class="tx-24 tx-white tx-lato tx-bold mg-b-2 lh-1"><?php  echo $compteConvoisAttente; ?></p>
                                <span class="tx-11 tx-roboto tx-white-6"></span>
                            </div>
                        </div>
                    </a></div>
                </div>
                <!-- col-3 -->
                <div class="col-sm-6 col-xl-3 mg-t-20 mg-xl-t-0">
                   <a href="" style="text-decoration:none;"><div class="bg-br-primary rounded overflow-hidden">
                        <div class="pd-25 d-flex align-items-center">
                            <i class="fa fa-clock tx-60 lh-0 tx-white op-7"></i>
                            <div class="mg-l-20">
                                <p class="tx-17 tx-spacing-1 tx-mont  tx-bold tx-uppercase tx-white-8 mg-b-10">Horloge</p>
                                <p class="tx-24 tx-white tx-lato tx-bold mg-b-2 lh-1"></p>
                                <span class="tx-11 tx-roboto tx-white-6"><b style="color:white"></b><span style="color:white" id="datetime"></span> </span>
                            </div>
                        </div>
                      </div>
                  </a></div>
                <!-- col-3 -->
            </div>
            <!-- row -->