<?php

session_start();
include('../../../server.php');

$id = $_GET["ref_affectations"];


$con = Server::connexion();

$con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

if($id!='')
{
$red=$con->prepare("SELECT * FROM affectations
 LEFT JOIN utilisateurs ON affectations.utilisateurs_affectation_id=utilisateurs.id_utilisateurs
 WHERE id_affectations=:A"
 ); 

$red->execute(array('A'=>$id));
$util=$red->fetch();

$id_comp = $util['compagnie_id'];

$req_comp=$con->prepare("SELECT * FROM `compagnie` WHERE id_compagnie=:A");
$req_comp->execute(array('A'=>$id_comp));
$comp=$req_comp->fetch();

$stit_st='Supprimer';

$_SESSION['id_affectations_mod']=$util["id_affectations"];
//}
?>

<form action="#"  enctype="multipart/form-data" id="form_modif_affectations">
                <div class="modal-body pd-20">
                <div class="row">
                              <div class="col-md-3">
                                <div class="form-group">
                                <label for="">Nom de la personne<span style="color: red;">*</span></label>
                                <select name="utilisateurs_modif" id="utilisateurs_modif" class="form-control" required>
                                     <option value="">---choisir---</option>
                                <?php
              
                               // $query="SELECT * FROM utilisateurs WHERE id_utilisateurs!='0'  AND (compagnie_id!='0' AND  compagnie_id!='')";
                               if ($_SESSION['role']=='3') {
                                $query="SELECT * FROM utilisateurs WHERE id_utilisateurs!='0' AND (compagnie_id!='0' AND  compagnie_id!='')";
                                } else {
                                    $query="SELECT * FROM utilisateurs WHERE id_utilisateurs!='0' AND compagnie_id='".$_SESSION['id_compagnie']."' AND id_utilisateurs!='".$_SESSION['id_session_user']."' ";
                                }
                                
                                $user = Server::Allquery($query,$con); 
                                foreach ($user as $users) {  ?>
                                     <option value="<?php echo $users['id_utilisateurs'] ?>" <?php if ($util['utilisateurs_affectation_id']==$users['id_utilisateurs']) { echo 'selected'; } ?>><?php echo $users['nom']." ".$users['prenoms'] ?></option>
                                <?php } ?>
                                </select>
                                </div>
                              </div>
                              <div class="col-md-4">
                              <div class="form-group">
                                <label for="">Téléphone <span style="color: red;"></span></label>
                                <input type="text" class="form-control" name="telephone" id="telephone_m" style="background-color: #FEF9E7;"  readonly="true" value="<?php echo $util['telephone'] ?>"  readonly="true">
                                </div>
                              </div>
                              <div class="col-md-4">
                              <div class="form-group">
                                <label for="">Email  <span style="color: red;"></span></label>
                                <input type="text" class="form-control" name="email" id="email_m" style="background-color: #FEF9E7;"  value="<?php echo $util['email'] ?>"  readonly="true">
                                </div>
                              </div>

                              <div class="col-md-4">
                              <div class="form-group">
                                <label for="">Compagnie  <span style="color: red;"></span></label>
                                <input type="text" class="form-control" name="compagnie_m" style="background-color: #FEF9E7;"  id="compagnie_m" value="<?php echo $comp['nom_compagnie']; ?>" readonly="true">
                                </div>
                              </div>

                              <div class="col-md-4">
                                <div class="form-group">
                                  <label for="">Gare  <span style="color: red;">*</span> </label>
                                  <select name="gare_id_mod" id="gare_id_mod" class="form-control">
                                      <option value="">---choisir---</option>
                                      <?php $query2="SELECT * FROM gare WHERE id_gare!='0'"; $gare_ = Server::Allquery($query2,$con); 
                                  foreach ($gare_ as $gares_) {  ?>
                                      <option value="<?php echo $gares_['id_gare']; ?>"  <?php if ($util['gare_affectation_id']==$gares_['id_gare']) { echo 'selected'; } ?>> <?php echo $gares_['nom_gare'] ?></option>
                                  <?php } ?>
                                  </select>
                                  </div>
                              </div>
                         
                         </div>     
                         
                </div><!-- modal-body -->
                <div class="modal-footer">
                  <button type="submit" id="btn_modif_affectations" class="btn btn-primary tx-size-xs"><i class="fas fa-save"></i> Modifier</button>
                  <button type="button" class="btn btn-danger tx-size-xs" data-dismiss="modal"><i class="fas fa-times"></i> Fermer</button>
                </div>
              </form> 



    <script>
        $('#form_modif_affectations').on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            cache: false,
            contentType: false,
            processData: false,
            url: "mod_affectation.php",
            data: new FormData(this),
            success: function(msg) {
             //alert(msg);
                if (msg == 1) {
                //  alert(msg);
                  var title="Erreur", message="Cette affectations existe déjà", type="error";
                    addtoast(title, message, type);
                }else if (msg == 2) {
                    var title="Erreur un champs est vide", message="Veuillez remplir le champs vide", type="error";
                   addtoast(title, message, type);
                } else {
                   // alert(msg);
                    $("div.msg_erreur").hide();
                    $('#myModal_affectations_mod').modal('toggle');
                    var title="Modification affectation", message="affectation modifiée avec succès", type="success";
                    Swal.fire({
                    position: 'top-end',
                    icon: type,
                    title: message,
                    showConfirmButton: false,
                    timer: 1500
                  })
                  
                  setTimeout(function() {
                       location.reload();
                    }, 2500)
                    
                   // addtoast(title, message, type);

                }
            }
        });

        return false;

    });


    $('#utilisateurs_modif').on('change', function() {
    //alert();
    $.ajax({
        type: "POST",
        url: "recup_info_personne.php",
        dataType:"json",
        data:"user_id=" + $(this).val(),
        success: function(res) {
          result = res;
        //  Object.values(res)
            //console.log(result);
           // console.log(result.tel);
              $('#telephone_m').val(result.tel);
              $('#email_m').val(result.email);
              $('#compagnie_m').val(result.comp);
        }
    });

    return false;

});


    function addtoast(title, message, type){
$.Toast(title, message, type, {
                    has_icon:true,
                    has_close_btn:true,
					stack: true,
                    fullscreen:true,
                    timeout:8000,
                    sticky:false,
                    has_progress:true,
                    rtl:false,
                });

            }
    //fin modifier
</script>
    
<?php } ?>              

 