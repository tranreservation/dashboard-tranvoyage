$(function() {
    'use strict'
    alert();
    // FOR DEMO ONLY 
    // menu collapsed by default during first page load or refresh with screen
    // having a size between 992px and 1299px. This is intended on this page only
    // for better viewing of widgets demo.
    $(window).resize(function() {
        minimizeMenu();
    });

    minimizeMenu();

    function minimizeMenu() {
        if (window.matchMedia('(min-width: 992px)').matches && window.matchMedia('(max-width: 1299px)').matches) {
            // show only the icons and hide left menu label by default
            $('.menu-item-label,.menu-item-arrow').addClass('op-lg-0-force d-lg-none');
            $('body').addClass('collapsed-menu');
            $('.show-sub + .br-menu-sub').slideUp();
        } else if (window.matchMedia('(min-width: 1300px)').matches && !$('body').hasClass('collapsed-menu')) {
            $('.menu-item-label,.menu-item-arrow').removeClass('op-lg-0-force d-lg-none');
            $('body').removeClass('collapsed-menu');
            $('.show-sub + .br-menu-sub').slideDown();
        }
    }
});

$('#datatable1').DataTable({
    //"scrollY": 200,
    "scrollX": true,
    responsive: false,
    language: {
        searchPlaceholder: 'Recherche...',
        sSearch: '',
        lengthMenu: '_MENU_ éléments/page',
    }
});

// Select2
//  $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
// alert();

// alert()
//Gestion des utilisateurs
$('#aff_compaganie').hide();
//Ajouter un utilisateur
$('#form_utilisateur').on('submit', function(e) {
    e.preventDefault();
    //alert();
    $.ajax({
        type: "POST",
        cache: false,
        contentType: false,
        processData: false,
        url: "ajout_utilisateur.php",
        data: new FormData(this),
        success: function(msg) {
            //  alert(msg);
            if (msg == 1) {
                var title = "Erreur",
                    message = "Cet utilisateur existe déjà",
                    type = "error";
                addtoast(title, message, type);
            } else {
                // alert(msg);
                $("div.msg_erreur").hide();
                $('#ModalajoutUser').modal('toggle');
                // $("#ajout-user-succes").modal('show');
                //setTimeout(8000,location.reload())
                var title = "Création utilisateur",
                    message = "Utilisateur enregistré avec succès",
                    type = "success";
                Swal.fire({
                    position: 'top-end',
                    icon: type,
                    title: message,
                    showConfirmButton: false,
                    timer: 1500
                })

                setTimeout(function() {
                    location.reload();
                }, 2500)

                // addtoast(title, message, type);

            }
        }
    });
    // }
    return false;

});



$("#type_utilisateur").change(function() {
    var val = $(this).val();
    if (val == 1) {
        //alert(val);
        $('#aff_compaganie').show();
        $('#compagnie').attr('required', true);
    } else {
        $('#aff_compaganie').hide();
        $('#compagnie').attr('required', false);
    }
});



$(".statusUser").on('click', function() {
    // alert()
    var id = $(this).attr('data-id');
    //alert(id)
    $.ajax({
        type: 'GET',
        url: 'getstatus_utilisateur.php',
        data: 'ref_user=' + id,
        success: function(msg) {
            //alert(msg);
            $(".aff_status").html(msg);
        }
    });

})


$('#btn_status').on('click', function() {
    // alert();
    $.ajax({
        type: "POST",
        url: "status_utilisateur.php",
        success: function(msg) {
            // alert(msg);
            if (msg == 1) {
                $("div.msg_erreur").show();
                $("div.msg_erreur").html("impossible de supprimer").show();
            } else {
                $("#myModal_utilisateur_etat").toggle();
                var title = "Vérrouillage utilisateur",
                    message = "Opération éffèctuée avec succès",
                    type = "success";
                Swal.fire({
                    position: 'top-end',
                    icon: type,
                    title: message,
                    showConfirmButton: false,
                    timer: 1500
                })

                setTimeout(function() {
                    location.reload();
                }, 2000)

            }
        }
    });

    return false;

});


//supprimer utilisateur
$(".supUser").on('click', function() {
    // alert()
    var id = $(this).attr('data-id');
    //alert(id)
    $.ajax({
        type: 'GET',
        url: 'getsup_utilisateur.php',
        data: 'ref_user=' + id,
        success: function(msg) {
            //alert(msg);
            $(".aff_sup").html(msg);
        }
    });

})


$('#btn_sup').on('click', function() {
    // alert();
    $.ajax({
        type: "POST",
        url: "sup_utilisateur.php",
        success: function(msg) {
            // alert(msg);
            if (msg == 1) {
                $("div.msg_erreur").show();
                $("div.msg_erreur").html("impossible de supprimer").show();
            } else {
                $("#myModal_utilisateur_sup").toggle();
                var title = "Vérrouillage utilisateur",
                    message = "Suppression éffèctuée avec succès",
                    type = "warning";
                Swal.fire({
                    position: 'top-end',
                    icon: type,
                    title: message,
                    showConfirmButton: false,
                    timer: 1500
                })

                setTimeout(function() {
                    location.reload();
                }, 2000)

            }
        }
    });

    return false;

});

//fin supprimer 

//modifier utilisateur
$(".modUser").on('click', function() {
    //alert()
    var id = $(this).attr('data-id');
    //alert(id)
    $.ajax({
        type: 'GET',
        url: 'getmodif_utilisateur.php',
        data: 'ref_user=' + id,
        success: function(msg) {
            //alert(msg);
            $(".aff_mod").html(msg);
        }
    });

})




function addtoast(title, message, type) {
    $.Toast(title, message, type, {
        has_icon: true,
        has_close_btn: true,
        stack: true,
        fullscreen: true,
        timeout: 8000,
        sticky: false,
        has_progress: true,
        rtl: false,
    });
    //location.reload();
}
//Fin ajout des utiilisateurs==============================




//Fin Gestion des utilisateurs