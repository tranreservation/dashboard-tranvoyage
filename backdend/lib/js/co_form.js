$(function() {

    //alert()
    $("div.div_load").hide();
    $("div.msg_error").hide();
    $("div.msg_ok").hide();

    $("#ident").val('');
    $("#passe").val('');
    $("#email").val('');


    $('#form_con').on('submit', function(event) {
        event.preventDefault();
        if ($("#ident").val() == '' || $("#passe").val() == '') {
            $("div.msg_error").html('<span>&nbsp;&nbsp;Entrez l&rsquo;identifiant et le mot de passe.</span>').show();
            setTimeout(function() {
                $("div.msg_error").hide();
            }, 4000);
        } else {
            $.ajax({
                type: "POST",
                url: "connexion.php",
                data: "ident=" + $("#ident").val() + "&pass=" + $("#passe").val(),
                success: function(msg) {
                    console.log(msg)
                    if (msg == 3) {
                        // $("div.msg_error").html('<span><img src="images/erreur.png" style="width:20px; height:20px;" />&nbsp;&nbsp;L&rsquo;identifiant ou le mot de passe est invalide.</span>').show();
                        // setTimeout(function() {
                        //     $("div.msg_error").hide();
                        // }, 4000);
                        var title = "Erreur",
                            message = "L&rsquo;identifiant ou le mot de passe est invalide",
                            type = "error";
                        addtoast(title, message, type);
                    } else if (msg == 6) {

                        // $("div.msg_error").html('<span><img src="images/erreur.png" style="width:20px; height:20px;" />D&eacute;sol&eacute; votre compte est désactiv&eacute;</span>').show();
                        // setTimeout(function() {
                        //     $("div.msg_error").hide();
                        // }, 4000);
                        var title = "Erreur",
                            message = "D&eacute;sol&eacute; votre compte est désactiv&eacute;",
                            type = "error";
                        addtoast(title, message, type);
                    } else {
                        $("div.msg_error").hide();
                        $("#btn_submit").html('Connexion en cours...').show();
                        setTimeout(function() {
                            $("div.msg_error").hide();
                            $(location).attr('href', "../../index.php");
                        }, 4000);
                    }
                }
            });

        }

        return false;

    });


    $('#form_mot').on('submit', function(event) {
        event.preventDefault();
        $.ajax({
            type: "POST",
            url: "recup_email.php",
            data: "email=" + $("#email").val(),
            success: function(msg) {
                if (msg == 3) {
                    $("div.msg_ok").hide();
                    var title = "Erreur",
                        message = "D&eacute;sol&eacute; cette adresse n'existe pas dans notre base de donn&eacute;e",
                        type = "error";
                    addtoast(title, message, type);
                } else {
                    $("div.msg_error").hide();
                    $("div.msg_ok").html('Réinitialisation en cours...').show();
                    setTimeout(function() {
                        $("div.msg_ok").hide();
                        $("div.msg_error").hide();
                        //      $(location).attr('href', "confirmation_envoie.php");
                        var title = "Mot de passe r&eacuteinitialis&eacute avec succes",
                            message = "Mot de passe r&eacuteinitialis&eacute avec succes, un email vient de vous etes envoy&eacute a cette adresse",
                            type = "success";
                        addtoast(title, message, type);
                    }, 4000);



                }
            }
        });

        return false;
    });



    $('#form_mod').on('submit', function(event) {
        event.preventDefault();
        if ($("#n_passe").val() == '' && $("#c_passe").val() == '') {
            $("div.msg_error").html('<span><img src="images/atten.png" style="width:35px; height:35px;" />&nbsp;&nbsp;Veillez entrer les mots de passe.</span>').show();
            setTimeout(function() {
                $("div.msg_error").hide();
            }, 4000);
        } else if ($("#n_passe").val() != $("#c_passe").val()) {
            $("div.msg_error").html('<span><img src="images/atten.png" style="width:35px; height:35px;" />&nbsp;&nbsp;Les mots de passe ne sont pas identiques.</span>').show();
            setTimeout(function() {
                $("div.msg_error").hide();
            }, 4000);
        } else {
            $.ajax({
                type: "POST",
                url: "modif_passe.php",
                data: "c_pas=" + $("#c_passe").val(),
                success: function(msg) {

                    if (msg == 0) {
                        $("div.msg_error").hide();
                        $("div.msg_ok").html('<span><img src="images/ok.png" style="width:38px; height:38px;" />&nbsp; Modification effectu&eacute;e.</span>').show();
                        $("#n_passe").val('');
                        $("#c_passe").val('');
                        $(location).attr('href', "../../connexion/login.php");
                    } else {
                        $("div.msg_error").html('<span><img src="images/erreur.png" style="width:35px; height:35px;" />&nbsp;&nbsp;Veillez contacter l&rsquo;administrateur.</span>').show();
                        setTimeout(function() {
                            $("div.msg_error").hide();
                        }, 4000);

                    }
                }

            });

        }

        return false;

    });

    function addtoast(title, message, type) {
        $.Toast(title, message, type, {
            has_icon: true,
            has_close_btn: true,
            stack: true,
            fullscreen: true,
            timeout: 8000,
            sticky: false,
            has_progress: true,
            rtl: false,
        });

    }

});