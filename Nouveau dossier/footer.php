
<footer class="footer-area ">
        <div class="container ">
            <div class="row justify-content-between ">
                <div class="col-sm-6 col-md-5 ">
                    <div class="single-footer-widget ">
                        <h4>Les destinations</h4>
                        <ul>
                            <li><a href="# ">Abidjan, Bouaké</a></li>
                            <!-- <li><a href="# ">California, USA</a></li>
                            <li><a href="# ">London, UK</a></li>
                            <li><a href="# ">Saintmartine, Bangladesh</a></li>
                            <li><a href="# ">Mount Everast, India</a></li>
                            <li><a href="# ">Sidney, Australia</a></li>
                            <li><a href="# ">Miami, USA</a></li>
                            <li><a href="# ">California, USA</a></li>
                            <li><a href="# ">London, UK</a></li>
                            <li><a href="# ">Saintmartine, Bangladesh</a></li>
                            <li><a href="# ">Mount Everast, India</a></li>
                            <li><a href="# ">Sidney, Australia</a></li> 12 villes -->

                            <!-- Abidjan
	Man
	Gagnoa
	Soubré
	Agbovill4
	Dabou
	Grand-Ba
	Bouaflé
	Issia
	Sinfra
	Katiola
	Bingerville
	Adzopé
	Séguéla
	Bondouko1
	Oumé
	Ferkesse
	Dimbokro3
	Odienné	
	Duékoué
	Danané
	Tingréla
	Guiglo
	Boundial
	Agnibilé 
	Daoukro
	Vavoua
	Zuénoula19 
	Tiassalé
	Toumodi	
	Akoupé	
	Lakota	 -->
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 ">
                    <div class="single-footer-widget ">
                        <h4>INSCRIPTION À LA NEWSLETTER</h4>
                        <div class="form-wrap " id="mc_embed_signup ">
                            <form target="_blank " action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01 " method="get " class="form-inline ">
                                <input class="form-control " name="EMAIL " placeholder="Votre adresse email" onfocus="this.placeholder='' " onblur="this.placeholder='Votre adresse email ' " required=" " type="email ">
                                <button class="click-btn btn btn-default text-uppercase "> <i class="far fa-paper-plane "></i>
</button>
                                <div style="position: absolute; left: -5000px; ">
                                    <input name="b_36c4fd991d266f23781ded980_aefe40901a " tabindex="-1 " value=" " type="text ">
                                </div>
                                <div class="info "></div>
                            </form>
                        </div>
                        <p>Abonnez-vous à notre newsletter pour recevoir les dernières nouvelles et offres</p>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3 ">
                    <div class="single-footer-widget footer_icon ">
                        <h4>NOUS CONTACTER</h4>
                        <p>4156, New garden, New York, USA +880 362 352 783</p>
                        <span><a href="" class="__cf_email__ " data-cfemail="f695999882979582b69b9784829f9893d895999b ">[email&#160;protected]</a></span>
                        <div class="social-icons ">
                            <a href="# "><i class="ti-facebook "></i></a>
                            <a href="# "><i class="ti-twitter-alt "></i></a>
                            <a href="# "><i class="ti-pinterest "></i></a>
                            <a href="# "><i class="ti-instagram "></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid ">
            <div class="row justify-content-center ">
                <div class="col-lg-12 ">
                    <div class="copyright_part_text text-center ">
                        <p class="footer-text m-0 ">
                            Copyright &copy;
                            <script data-cfasync="false " src="cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js "></script>
                            <script>
                                document.write(new Date().getFullYear());
                            </script> Tous droits réservés | Développer par <a href="#" target="_blank ">Romeo Osse</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>