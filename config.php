<?php
$pageURL = 'http';
if ($_SERVER["HTTPS"] == "on")
{
	$pageURL .= "s";
}
$pageURL .= "://";
$url1 = $_SERVER["SERVER_NAME"];
$params = explode('.', $url1);
if($pageURL!="https://")
{
	if(sizeof($params) === 3 AND $params[0] == 'www')
	{
		$pageURL ="https://". $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		header("Location: ".$pageURL);
	}
	else
	{
		$pageURL ="https://www.". $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		header("Location: ".$pageURL);
	}
}
else
{
	if(sizeof($params) === 3  AND $params[0] == 'www')
	{
	}
	else{
		$pageURL ="https://www.". $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		header("Location: ".$pageURL);
	}
}


// Toggle this to change the setting
define('DEBUG', false);

// You want all errors to be triggered
error_reporting(E_ALL);

if(DEBUG == true)
{
    // You're developing, so you want all errors to be shown
    ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
}
else
{
    ini_set('display_errors', 0);
	ini_set('display_startup_errors', 0);
	error_reporting(E_ALL);
}

opcache_reset();
?>